Title: sous clé avec PGP
Date: 2018-01-12 08:34
Author: mazenovi
Category: vie-privee
Tags: open-source, linux
Status: draft
Slug: sous-cle-avec-gpg


Batman se sert de gpgenv pour se créer une paire de clés PGP

```
$ wget http://m4z3.me/gpgenv -O /tmp/gpgenv
$ source  /tmp/gpgenv
$ gpgenv create
$ gpgenv cd
$ gpg --full-gen-key
```

```
$ gpg --list-secret-keys
/tmp/gnupg-GPA1A/pubring.kbx
----------------------------
sec   rsa2048/84A82584 2018-03-05 [SC]
uid        [  ultime ] Batman (Adresse de super héros) <batman@batcave.com>
ssb   rsa2048/8B65EE91 2018-03-05 [E]

```

Batman a une clé pour signer et certifier et une sous clé de chiffrement.

Le mot de passe de cette clé maître est "master"

```
$ gpg --edit-key batman@batcave.com
gpg (GnuPG) 2.1.11; Copyright (C) 2016 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

La clef secrète est disponible.

sec  rsa2048/84A82584
     créé : 2018-03-05  expire : jamais       utilisation : SC  
     confiance : ultime        validité : ultime
ssb  rsa2048/8B65EE91
     créé : 2018-03-05  expire : jamais       utilisation : E   
[  ultime ] (1). Batman (Adresse de super héros) <batman@batcave.com>

gpg> addkey
Sélectionnez le type de clef désiré :
   (3) DSA (signature seule)
   (4) RSA (signature seule)
   (5) Elgamal (chiffrement seul)
   (6) RSA (chiffrement seul)
Quel est votre choix ? 4
les clefs RSA peuvent faire une taille comprise entre 1024 et 4096 bits.
Quelle taille de clef désirez-vous ? (2048)
La taille demandée est 2048 bits
Veuillez indiquer le temps pendant lequel cette clef devrait être valable.
         0 = la clef n'expire pas
      <n>  = la clef expire dans n jours
      <n>w = la clef expire dans n semaines
      <n>m = la clef expire dans n mois
      <n>y = la clef expire dans n ans
Pendant combien de temps la clef est-elle valable ? (0) 2y
La clef expire le mer. 04 mars 2020 23:11:27 CET
Est-ce correct ? (o/N) o
Faut-il vraiment la créer ? (o/N) o
De nombreux octets aléatoires doivent être générés. Vous devriez faire
autre chose (taper au clavier, déplacer la souris, utiliser les disques)
pendant la génération de nombres premiers ; cela donne au générateur de
nombres aléatoires une meilleure chance d'obtenir suffisamment d'entropie.

sec  rsa2048/84A82584
     créé : 2018-03-05  expire : jamais       utilisation : SC  
     confiance : ultime        validité : ultime
ssb  rsa2048/8B65EE91
     créé : 2018-03-05  expire : jamais       utilisation : E   
ssb  rsa2048/C518ACE2
     créé : 2018-03-05  expire : 2020-03-04  utilisation : S   
[  ultime ] (1). Batman (Adresse de super héros) <batman@batcave.com>
```

Batman rentre le mot de passe de la master key pour la modifier et le mot de passe qu'il veut pour la sous clé.

```
gpg> save
```

Batman a maintenant une sous clé de signature dont le mot de passe est "sign" dans son certificat.

C'est le bon moment pour exporter son certificat et sa clé privée

```
$ gpg --export --armor batman@batcave.com > batman.pub.asc
$ gpg --export-secret-keys --armor batman@batcave.com > batman.priv.asc
```

Si maintenant Batman veut désactiver sa clé maître en vue d'utiliser sa clé de signature seule sur son mobile, il va taper [la commande du cours](https://fc.isima.fr/~mazenod/slides/privacy/pgp.html#/0/20)

```
$ gpg --delete-secret-key batman@batcave.com
sec  rsa2048/84A82584 2018-03-05 Batman (Adresse de super héros) <batman@batcave.com>

Faut-il supprimer cette clef du porte-clefs ? (o/N)
```

En vérifiant il se rend compte que c'est bien l'id de sa clé maître (69D27A55) donc il répond oui car c'est bien ce qu'il veut désactiver

```
C'est une clef secrète — faut-il vraiment la supprimer ? (o/N)
```

oui c'est pour ça que je veux la supprimer même: jé réponds oui

Ensuite viennent un série de fenêtre système et il faut bien lire ce qui est écrit:
Chaque fenêtre est une fenêtre de confirmation pour une sous clé:
- d'abord 84A82584 il confirme
- ensuite 8B65EE91 il confirme
- ensuite C518ACE2 il ne confirme pas c'est précisément la clé qu'il veut garder

A ce stade il ne lui reste qu'une sous clé de signature dans le certificat (les autres sont désactivées ce qui est symbolisé par le #)

```
$ gpg --list-secret-keys
/tmp/gnupg-GPA1A/pubring.kbx
----------------------------
sec#  rsa2048/84A82584 2018-03-05 [SC]
uid        [  ultime ] Batman (Adresse de super héros) <batman@batcave.com>
ssb#  rsa2048/8B65EE91 2018-03-05 [E]
ssb   rsa2048/C518ACE2 2018-03-05 [S] [expire : 2020-03-04]

```

Il exporte ce certificat en vue de le mettre sur un device quelconque (la clé maître n'est pas dans le certificat donc pas volable)

```
$ gpg --export-secret-keys --armor batman@batcave.com > batman.mobile.asc
```

Notez que pour utiliser cette sous clé de signature (et ici pour l'exporter), il n'a qu'à saisir son mot de passe et plus celui de la clé maître (normal : elle n'est plus là!)

Il fait un backup

```
gpgenv backup ~/Bureau/batman.tgz
```

et je me garde les fichiers de Batman sur mon Bureau

```
$ mkdir ~/Bureau/batman
$ tar -xvzf ~/Bureau/batman.tgz -C ~/Bureau/batman/
```

Je mets aussi sur mon bureau ma clé publique et ma clé privée perso ```taulier@m4z3.me.pub.asc``` et ```taulier@m4z3.me.priv.asc``` (vous pouvez suivre la suite en remplaçant par votre paire de clés)

Batman se créée un environnement pgp vierge et importe la sous clé de signature isolée

```
$ gpgenv create
$ gpgenv cd
$ gpg --import  ~/Bureau/batman/batman.mobile.asc
```

la question est : Que peut faire Batman avec juste une clé de signature

disons que qu'il importe une clé publique que je maîrtise (~/Bureau/taulier@m4z3.me.pub.asc)


```
$ gpg --import ~/Bureau/taulier@m4z3.me.pub.asc
```

```
$ gpg --list-keys
/tmp/gnupg-6uQN5/pubring.kbx
----------------------------
pub   rsa2048/84A82584 2018-03-05 [SC]
uid        [ inconnue] Batman (Adresse de super héros) <batman@batcave.com>
sub   rsa2048/8B65EE91 2018-03-05 [E]
sub   rsa2048/C518ACE2 2018-03-05 [S] [expire : 2020-03-04]

pub   rsa2048/9C96F4C5 2014-12-17 [SCA] [expire : 2019-12-16]
uid        [ inconnue] Vincent Mazenod (clef PGP pour m4z3.me) <taulier@m4z3.me>
sub   rsa2048/8979C7D4 2014-12-17 [E] [expire : 2019-12-16]
```

 et en clé secrète il n'a toujours que sa sous clé de signature

```
$ gpg --list-secret-keys
/tmp/gnupg-6uQN5/pubring.kbx
----------------------------
sec#  rsa2048/84A82584 2018-03-05 [SC]
uid        [ inconnue] Batman (Adresse de super héros) <batman@batcave.com>
ssb#  rsa2048/8B65EE91 2018-03-05 [E]
ssb   rsa2048/C518ACE2 2018-03-05 [S] [expire : 2020-03-04]
```

Puis Batman chiffre un message pour taulier@m4z3.me

```
$ echo "hello maze" > ~/Bureau/msg
$ gpg --encrypt --armor --output ~/Bureau/msg.pgp --recipient taulier@m4z3.me ~/Bureau/msg
gpg --encrypt --armor --output ~/Bureau/msg.pgp --recipient taulier@m4z3.me ~/Bureau/msg
gpg: 8979C7D4 : aucune assurance que la clef appartienne vraiment à l'utilisateur nommé.
sub  rsa2048/8979C7D4 2014-12-17 Vincent Mazenod (clef PGP pour m4z3.me) <taulier@m4z3.me>
 Empreinte clef princip. : ACE7 302C DD02 2220 689F  4DDB FC31 7760 9C96 F4C5
      Empreinte de sous-clef : B30C 5178 BC41 B56F B73C  618D FEF8 824F 8979 C7D4

La clef n'appartient PAS forcément à la personne nommée
dans l'identité. Si vous savez *vraiment* ce que vous
faites, vous pouvez répondre oui à la prochaine question.

Faut-il quand même utiliser cette clef ? (o/N) o
```

Notez qu'on ne me demande ici aucun mot de passe (donc aucune clé n'est utilisée)

Ensuite Batman signe

```
$ gpg --output ~/Bureau/msg.sig --detach-sig ~/Bureau/msg
```

Notez qu'ici Batman doit saisir le mot de passe de sa sous clé de signature ("sign")

Maintenant je vais recréer un environnement PGP vierge et voir si je lis le message avec ma clé ~/Bureau/taulier@m4z3.me.priv.asc et si je peux vérifier la signature

```
$ gpgenv create
$ gpgenv cd
$ gpg --import ~/Bureau/taulier@m4z3.me.priv.asc
```

je saisis le mot de passe de ma clé maître et

```
$ gpg --list-secret-keys
/tmp/gnupg-BNqoZ/pubring.kbx
----------------------------
sec   rsa2048/9C96F4C5 2014-12-17 [SCA] [expire : 2019-12-16]
uid        [ inconnue] Vincent Mazenod (clef PGP pour m4z3.me) <taulier@m4z3.me>
ssb   rsa2048/8979C7D4 2014-12-17 [E] [expire : 2019-12-16]
```

Pour déchiffrer le message de Batman

```
$ gpg --decrypt ~/Bureau/msg.pgp > msg
gpg: chiffré avec une clef RSA de 2048 bits, identifiant 8979C7D4, créée le 2014-12-17
      « Vincent Mazenod (clef PGP pour m4z3.me) <taulier@m4z3.me> »
$ cat msg
hello maze
```

Ca a marché : Batman a chiffré un message pour moi sans utiliser une seule de ses clés uniquement à partir de ma clé publique (j'ai du tapé le mot de passe de ma clé privée maître pour déchiffrer le message)

je vérifie la signature de Batman

```
$ gpg --verify ~/Bureau/msg.sig msg
gpg: Signature faite le lun. 05 mars 2018 22:41:28 CET avec la clef DSA d'identifiant 3DD038FF
gpg: Impossible de vérifier la signature : Pas de clef publique
```

Ah oui il me faut la clé publique de Batman pour ça

```
$ gpg --import ~/Bureau/batman/batman.pub.asc
```

A priori c'est ok!

```
gpg: Signature faite le lun. 05 mars 2018 23:33:07 CET avec la clef RSA d'identifiant C518ACE2
gpg: Bonne signature de « Batman (Adresse de super héros) <batman@batcave.com> » [inconnu]
```

Même si je pense que j'aurais du truster cette sous clé

(note pour plus tard: faire disparaître ce warning)

```
gpg: Attention : cette clef n'est pas certifiée avec une signature de confiance.
gpg:          Rien n'indique que la signature appartient à son propriétaire.
Empreinte de clef principale : F98E 5438 C968 8719 895A  B183 9CFD 7813 84A8 2584
     Empreinte de la sous-clef : D95F B898 B372 25F3 3046  91D2 5E89 F9FB C518 ACE2
```

Tant que je suis là je vais faire coucou à Batman en signant mon message

```
$ echo "hello batman" > ~/Bureau/msg
$ gpg --encrypt --armor --output ~/Bureau/msg.pgp --recipient batman@batcave.com  ~/Bureau/msg
$ gpg --output ~/Bureau/msg.sig --detach-sig ~/Bureau/msg
```

Batman se créée un environnement vierge pour déchiffrer et vérifier la signature

```
$ gpgenv create
$ gpgenv cd
$ gpg --import ~/Bureau/batman/batman.mobile.asc
$ gpg --import ~/Bureau/taulier@m4z3.me.pub.asc
$ gpg --decrypt ~/Bureau/msg.pgp > msg
gpg: chiffré avec une clef RSA de 2048 bits, identifiant 8B65EE91, créée le 2018-03-05
      « Batman (Adresse de super héros) <batman@batcave.com> »
gpg: échec du déchiffrement : Pas de clef secrète
```

Batman n'a pas pu lire le message que j'ai chiffré pour lui car sa clé de chiffrement [E] a été désactivée dans le certififcat qu'il a dans son mobile.

Maintenant à quoi sert une clé de chiffrement

Maintenant à quoi sert une sous clé de chiffrement

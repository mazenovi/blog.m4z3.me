Title: La crypto facile avec TrueCrypt
Date: 2011-03-21 10:58
Author: mazenovi
Category: vie privée
Tags: open-source, linux, windows
Slug: la-crypto-facile-avec-truecrypt
Status: published

Pourquoi chiffrer?
=================

![crypto_cat](images/truecrypt/crypto_cat.gif){: align="left" style="margin: 15px;" }

C'est sûr on ne balade pas tous
des dossiers classés secrets défense dans nos ordinateurs portables.
Mais si on prend deux secondes pour y réfléchir, perdre ou se faire
voler une machine peut vite se révéler fort ennuyeux.

Les mots de passe sauvegardés pour la Messagerie, pour les instant
messengers (MSN, Gtalk), dans la saisie semi automatique du navigateurs
(facebook, twitter, webmail etc ..), sont autant de petits gains de
temps apparemment anodins qui deviendront tout à coup de véritables
vulnérabilités pouvant mettre en péril l'intégrité de votre identité
numérique. Quiconque trouve ou vole votre machine, peut (à minima) se
faire passer pour vous tant que vous n'avez pas changé les mots de passe
de tous les sevrices que vous utilisez ... si tant est que vous puissiez
le faire avant le voleur ...

Le b-a-ba est alors de protéger sa session par mot de passe ... ce qui
est  bien mais pas top, dans lême si la session est protégée par mot de
passe: il suffit d'accéder à ce disque à partir d'un autre système en le
branchant directement à la carte mère ou via une coque USB. Ce qui est
donc à la portée de n'importe qui!!

![laptop-privacy-1](images/truecrypt/laptop-privacy-1.jpg){: align="right" style="margin: 15px;" }

C'est là que le cryptage fait sens. En effet ce dernier permettra de
rendre toutes vos données inaccessibles tant que le mot de passe qui
permet de voir les données en claire n'a pas été saisi, et ce qu'on
accède au disque d'une autre machine ou non.

Chiffrer quoi?
=============

Je viens d'évoquer le cas de l'ordinateur portable qui est le cas
typique d'utilisation nomade, mais les mêmes considérations s'appliquent
aux suite d'applications portables
([FramaKey](http://www.framakey.org/),
[liberKey](http://www.liberkey.com/),[portableapps](http://portableapps.com/))
qui peuvent être utilisée sur clé ou disque dur USB.

Il faut donc considérer le cryptage des disques systèmes, aussi bien que
le cryptage des disques ou parties de disques des périphériques de
stockage externes.

Ca tombe bien [TrueCrypt](http://www.truecrypt.org/) gère tout ces cas
de figure

Comment chiffrer?
================

Sous windows il y a des solutions natives comme [bitlocker, mais elles
n'ont pas eu que des bons
échos](http://pro.01net.com/editorial/509777/le-chiffrement-de-bitlocker-chahute-par-les-experts/).
Selon les constructeurs il y a aussi des possibilités plus "hard", des
disques durs auto chiffrant par exemple (à noter qu'il existe la même
chose pour les clés USB). Et puis [sous linux il existe
LVM](http://www.korben.info/comment-chiffrer-une-partition-systeme-linux-ici-ubuntu.html)
([un autre tuto
ici](http://news.softpedia.com/news/Encrypted-Ubuntu-7-10-68383.shtml)).
Je n'ai pas vraiment creusé pour trouver quelque chose sous MAC, si vous
avez des suggestions les commentaires sont ouverts ...

!["52015_416_corsair_un_clef_avec_chiffrement_des_donnees"](images/truecrypt/52015_416_corsair_un_clef_avec_chiffrement_des_donnees.jpg){: align="left" style="margin: 15px;" }

La solution que propose [TrueCrypt](http://www.truecrypt.org/) existe
sur ces 3 OS. Outre le fait que vous pourrez suivre la même procédure
pour chacune de vos machines quelque soit son OS, vous allez aussi
pouvoir chiffrer vos disques durs portables et autres clés USB et les
utiliser sur n'importe quel OS.

[chiffrer son disque système
Windows](http://www.korben.info/chiffrer-un-disque-systeme-windows-avec-truecrypt.html)
est une opération relativement simple. Officiellement True Crypt ne
permet pas de réaliser cette opération sous linux. Plus chagrinant le
dual boot windows / linux sur un même disque n'est pas supporté et
empêche du coup de chiffrer la partition windows.

J'utilise surtout [TrueCrypt](http://www.truecrypt.org/) pour ma suite
d'application portable [LiberKey](http://www.liberkey.com/) qui est 
pour moi de plus en plus cruciale. Voulant profiter au maximum du
caractère portable de [TrueCrypt](http://www.truecrypt.org/) j'ai choisi
d'opter pour le fichier crypté plutôt que pour la partition cryptée, et
je pense que cette option est la plus ergonomique pour les médias
portables. En effet Si [TrueCrypt](http://www.truecrypt.org/) est à
installer au niveau de l'OS sous linux ou sous mac os, il existe une
version portable pour windows! Il est donc possible de se déplacer avec
[TrueCrypt](http://www.truecrypt.org/) sur le disque contenant le
fichier crypté et d'y accéder même sur une machine où
[TrueCrypt](http://www.truecrypt.org/) n'est pas installé ... et où
éventuellement on ne dispose pas des permissions suffisantes pour
réaliser cette opération

L'idée est donc de créer un fichier crypté qui tient presque toute la
place sur le disque (ici DataMaze)

<div style="text-align: center">
    <img src="images/truecrypt/screenshot.1.jpg">
</div>

et d'entreposer le nécessaire portable de TrueCrypt dans un répertoire à
côté

<div style="text-align: center">
    <img src="images/truecrypt/screenshot.2.jpg">
</div>

Il ne reste ensuite plus qu'à batcher le montage (sur le lecteur Z dans
notre cas) de la clé mount.bat

```cmd
 @echo off  
TrueCrypt\TrueCrypt /v DataMaze /l Z /m rm /auto /q
```

et le démontage unmount.bat

```cmd
TrueCrypt\TrueCrypt /d Z /q
```

L'un des inconvénients majeur de cette méthode est qu'il est possible
d'effacer le fichier crypté sans avoir d'autorisation spéciale :'(

Les perfs
======================

Le chiffrement est long quoiqu'il arrive: compter 100Go à l'heure.

En revanche les performances système ne sont quasi pas altérées au
niveau de l'utilisation des disques ou fichiers chiffrer. J'ai fait un
petit test sur un disque de 500Go avec
[CrystalDiskMark](http://crystalmark.info/software/CrystalDiskMark/index-e.html)

Avant
-----

```cmd
-----------------------------------------------------------------------  
CrystalDiskMark 3.0.1 (C) 2007-2010 hiyohiyo  
Crystal Dew World : http://crystalmark.info/  
-----------------------------------------------------------------------  
* MB/s = 1,000,000 byte/s [SATA/300 = 300,000,000 byte/s]

Sequential Read : 31.724 MB/s  
Sequential Write : 26.235 MB/s  
Random Read 512KB : 19.922 MB/s  
Random Write 512KB : 25.046 MB/s  
Random Read 4KB (QD=1) : 0.436 MB/s [ 106.5 IOPS]  
Random Write 4KB (QD=1) : 0.602 MB/s [ 147.1 IOPS]  
Random Read 4KB (QD=32) : 0.460 MB/s [ 112.4 IOPS]  
Random Write 4KB (QD=32) : 0.620 MB/s [ 151.4 IOPS]

Test : 1000 MB [D: 0.1% (0.7/465.8 GB)] (x5)  
Date : 2011/02/22 14:52:21  
OS : Windows 7 [6.1 Build 7600] (x86)
```

Après

```cmd
-----------------------------------------------------------------------  
CrystalDiskMark 3.0.1 (C) 2007-2010 hiyohiyo  
Crystal Dew World : http://crystalmark.info/  
-----------------------------------------------------------------------  
* MB/s = 1,000,000 byte/s [SATA/300 = 300,000,000 byte/s]

Sequential Read : 35.005 MB/s  
Sequential Write : 26.591 MB/s  
Random Read 512KB : 14.992 MB/s  
Random Write 512KB : 21.508 MB/s  
Random Read 4KB (QD=1) : 0.374 MB/s [ 91.3 IOPS]  
Random Write 4KB (QD=1) : 0.626 MB/s [ 152.9 IOPS]  
Random Read 4KB (QD=32) : 0.423 MB/s [ 103.4 IOPS]  
Random Write 4KB (QD=32) : 0.662 MB/s [ 161.7 IOPS]

Test : 1000 MB [Z: 0.5% (2.4/463.9 GB)] (x5)  
Date : 2011/02/23 15:40:31  
OS : Windows 7 [6.1 Build 7600] (x86)
```

Mot de passe vs Keyfile
=======================

Il y a deux systèmes d'authentification: Le système par mot de passe
classique et le système d'authentification par fichier. Le second
implique que vous ayez toujours le fichier d'authentification sous la
main (et tant qu'à faire pas sur le disque qui contient le conteneur
crypté :o)). Les deux systèmes sont combinables.

Notez qu'il n'y a pas de système de récupération de mot de passe, ni de
re-génération de fichier d'authentification ...

Mot de passe oublié  = données perdues. Ce qui est finalement assez
rassurant, si on y réfléchit bien!

Le mode parano?
===============

Il est possible de mettre en place un conteneur  caché, pour prévenir le
cas où on est forcé (ambiance couteau sous la gorge) de donner son mot
de passe. Le principe est assez simple, on appelle ça le déni plausible:
au lieu de créer une partition cryptée TrueCrypt en créera deux, l’une
dans l’autre. L'une qui fera office de leurre, alors que l'autre
contiendra effectivement les données confidentielles. L’idée est que
même si le mot de passe de la partition leurre est révélé il ne permet
d’accéder qu'à cette partition, et il est alors impossible de prouver
qu’il existe une deuxième partition cryptée (celle qui contient les
données confidentielles) dans la première.

le déni plausible peut être mis en oeuvre, pour un fichier crypté, une
partition et même un système tout entier.

plusieurs réflexions tout de même :

-   vous perdez un peu de place
-   ca fait un deuxième mot de passe à retenir (et à bien mémoriser
    parce qu'il faudra le sortir naturellement au moment où on vous
    braque un kalashnikov sur la tempe)
-   si vous avez mis  le déni plausible en place il y a un certain temps
    et que vous n'avez pas touché à la partition "fake" depuis, votre
    agresseur, s'il sait ce qu'il cherche et s'il sait regarder la date
    de dernière modification d'un fichier, aura vite fait de
    s'apercevoir que vous le prenez pour un lapin de 6 semaines ...

Tout récemment [Korben relayait le moyen de cacher un conteneur truecrypt
dans des fichiers
vidéo](http://www.korben.info/cacher-un-conteneur-truecrypt-dans-une-video.html),
ce qui me paraît beaucoup moins pratique à utiliser, mais beaucoup plus
crédible en terme de camouflage ;)

Conclusion
==========

Si vous lisez ces lignes et que vous vous dites que vous n'avez rien à
cacher et que chiffrer son disque ce n'est bon que pour [Julian
Assange](http://fr.wikipedia.org/wiki/Julian_Assange), c'est que j'ai
doublement perdu mon temps:

1 - il n'y a pas que les données confidentielles qui nécessitent un
cryptage, mais tout flux de données vous concernant peut potentiellement
être utilisé contre vous et / ou à votre insu. Ce qui implique que le
cryptage de données devrait être une pratique tout à fait commune dans
une société du tout numérique ... il n'y a qu'à lire les mésaventures
du [dragueur trahi par sa clef
USB](http://virtualabs.fr/spip.php?article45) pour s'en persuader

2 - C'est que vous pensez que chiffrer un système d'exploitation ou un
périphérique externe de stockage est compliqué, ce qui est tout à fait
faux, dans la mesure où [TrueCrypt](http://www.truecrypt.org/) s'occupe
de tout et qu'il existe de nombreuses ressources pour toute sorte de cas
pratiques

Liens complémentaires
---------------------

-   <http://free.korben.info/index.php/True_crypt>
-   <http://fr.wikipedia.org/wiki/TrueCrypt>

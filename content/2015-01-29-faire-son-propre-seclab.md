Title: Faire son propre Seclab
Date: 2015-01-29 14:23
Author: mazenovi
Category: sécurité
Tags: open-source, www, linux
Slug: faire-son-propre-seclab
Status: published

Imaginons que vous vouliez sèrieusement comprendre comment exploiter les vulnérabilités d'un serveur web.
Vous feriez mieux de vous entraîner sur l'un de vos serveur.

Mieux vous pouvez utiliser des machines virtuelles: vous serez alors en mesure de rejouer les attaques autant de fois que vous le souhaitez sans vous préoccuper de casser votre serveur.

Les machines virtuelles sont pratiques pour héberger des services vulnérables mais elles sont aussi pratiques pour héberger des systèmes offensifs de type Kali linux.

Il existe déjà pas mal de tutoriel qui explique comment monter des VMs et jouer des scenari d'attaques, celui ci explique

* une façon simple d'installer un OS offensif opérationnel
* une configuration réseau permettant de faire communiquer les VMs entre elles, l'hôte avec les VMs et les VMs avec le reste du monde (aka Internet)
* quelques pistes pour jouer des attaques qui claquent entre ces VMs

# Logiciels de virtualization

Sur le marché des gratuits on a deux leaders:
[VirtualBox](https://www.virtualbox.org/) & [VMware Workstation Player](https://my.vmware.com/fr/web/vmware/free#desktop_end_user_computing/vmware_workstation_player/12_0) beaucoup de distro citées par la suite sont disponibles dans l'une ou l'autre des versions. Certaines comme les images [metasploitable](http://sourceforge.net/projects/metasploitable/), sont WMPlayer uniquement ...

La reco est donc d'installer les deux. Nous verrrons par la suite comment faire communiquer toutes les VMs ensemble via les ponts réseaux créés par WMPlayer.

## VirtualBox

Je vous suggère de télécharger la dernière version directement à partir du [site officiel](https://www.virtualbox.org/wiki/Downloads).

Pour debian / Mint / Ubuntu l'installation devrait se résumer à une ligne de commande du type

~~~language-bash
sudo dpkg -i ~/Téléchargements/virtualbox-4.3_*.deb
~~~

Pour éviter l'installation des système hébergés il peut être judicieux de télécharger les images d'[OSboxes](http://www.osboxes.org/).

## Kali

Pour Kali il y a un image [OSboxes](http://www.osboxes.org/kali-linux/), mais le mieux est encore d'utiliser ["Offensive Security"'s Kali VMs](https://www.offensive-security.com/kali-linux-vmware-arm-image-download/).

Choisir une version de Kali peut être une question de goût: personnellement, à cette date ma préférence va encore à [Kali Linux 32 bit](http://images.kali.org/Kali-Linux-1.1.0a-vbox-486.7z). Plus légère que Kali 2 ( ca téte en ressource gnome 3), metasploit et OWASP Zap m'ont réservé quelques mauvaises surprises sur certaines attaques.

Les instruction suivantes s'appliquent aux autres versions également

_Les versions VMware sont aussi disponibles ;)_

on dézippe l'arhive téléchargée

~~~language-bash
7za e ~/7za e ~/Téléchargements/Kali_Linux_1.1.0-32bit.7z
~~~

* [Offensive Security VMs](https://www.offensive-security.com/kali-linux-vmware-arm-image-download/) se présente
    * sous la forme d'un unique fichier .ova file pour Kali 2
        * il suffit alors d'importer le fichier via Virtual Box et de démarrer la VM
    * sous la forme de plusieurs fichiers vdmk pour Kali 1.1
* [OSboxes Kali VMs](http://www.osboxes.org/kali-linux/) se présente
    * sous la forme d'un unique fichier vdi pour toutes les version de kali

Les commandes suivantes s'appliquent au fichier vdi d'[OSboxes](http://www.osboxes.org/kali-linux/) mais fonctionnent également avec les fichirs vdmk de [Offensive Security](https://www.offensive-security.com/kali-linux-vmware-arm-image-download/)

Ouvrez Virtual Box

![Kali Step 1](/images/seclab/Kali-s-1.png "Kali Step 1"){class="img-responsive center-block"}

créez une nouvelle VM

![Kali Step 2](/images/seclab/Kali-s-2.png "Kali Step 2"){class="img-responsive center-block"}

Allouez un peu de RAM

![Kali Step 3](/images/seclab/Kali-s-3.png "Kali Step 3"){class="img-responsive center-block"}

coupez / collez les fichiers de la VMs (un fichier vdi ou plusieurs fichiers vdmk) dans le répertoire Virtual Box

~~~language-bash
mv ~/Téléchargements/32bit/Kali Linux 1.1.0 (32bit).vdi ~/VirtualBox VMs/Kali/
~~~

naviguez ensuite jusuqu'à l'emplacement du fichier vdi (ou des fichiers vdmk) et terminez la création de la VM

![Kali Step 4](/images/seclab/Kali-s-4.png "Kali Step 4"){class="img-responsive center-block"}

La nouvelle VM peut maintenant être démarrée

Si le démarrage plante avec un "Guru mediation" (Ohh Amiga 500 je me souviens) activez l'option "PAE" dans "System" > "Processor"

![Kali Step PAE](/images/seclab/Kali-pae.png "Kali PAE"){class="img-responsive center-block"}

* username : root
* password : osboxes.org
(n'oubliez pas que vous êtes en mode qwerty ;)

__N.B. si vous utilisez les VM Security Offensive les login password sont__

* username : root
* password : toor

### 3 trucs à faire dès le début

#### Mettre le clavier français

"Applications" > "System Tools" > "Preferences" > "System" > "Region and Language" > "Layout tab"

- "+" pour ajoutéer
- tapez "fren" pour voir french, sélectionnez le, et cliquez pour l'ajouter

!["Kali Step Keyboard 1"](/images/seclab/Kali-k-1.png "Kali Step Keyboard 1"){class="img-responsive center-block"}

- cliquez sur la flèche "haut" pour mettre "french" tête de liste

![alt text](/images/seclab/Kali-k-2.png "Kali Step 4"){class="img-responsive center-block"}

Maintenant vous pouvez changer de langue à droite dans la barre des programmes (en haut)

Attention vous serez toujours en mode qwerty au démarrage te à la saisie du mot de passe. [Si vous voulez changer le clavier par défaut suivez de lien](https://wiki.debian.org/Keyboard)

#### Changer le mot de passe de root

~~~language-bash
passwd
~~~

#### installer les Guest additions

- Meilleur support vidro
- Buffer partagé entre systèmes hôte et invité
- Dossiers partagés entre systèmes hôte et invité
- Partage des périphériques USB

__N.B. SI vous vous utilisez Security Offensive VM vous pouvez directement installer les VirtualBox Guest Additions, otre systèem est prêt__

Pour [OSboxes Kali VMs](http://www.osboxes.org/kali-linux/) ajoutez au système /etc/apt/sources.list

~~~language-bash
deb http://http.kali.org/kali kali-current main non-free contrib
deb-src http://http.kali.org/kali kali-current main non-free contrib
~~~

puis exécutez les commandes suivantes

~~~language-bash
apt-get update
apt-get upgrade -y
apt-get dist-upgrade -y
reboot
apt-get install linux-headers-$(uname -r)
~~~

montez l'image des Guest Additions

~~~language-bash
mount VirtualBox Guest Additions drive from VirtualBox menu
~~~

copiez puis exécuter le script d'installation

~~~language-bash
cp /media/cdrom/VBoxLinuxAdditions.run /root/Desktop/
cd /root/Desktop/
chmod 755 VBoxLinuxAdditions.run
./VBoxLinuxAdditions.run
~~~

## VMPlayer

[télécharger la dernière version](https://my.vmware.com/fr/web/vmware/free)

__N.B. si vous avez un système 32 bits vous devrez utiliser la version 6.0 mais ça marche pareil :D__

~~~language-bash
sudo chmod 755 VMware-Player-6.0.6-2700073.i386.bundle
sudo ./VMware-Player-6.0.6-2700073.i386.bundle
~~~

Si vous téléchargez maintenant [OWASP Broken Web Applications Project](https://www.owasp.org/index.php/OWASP_Broken_Web_Applications_Project#tab=Main) par exemple

* déplacer le fichier décompressé dans ~/.vmware/OWASP_BWA
* "File" > "Open a virtual Machine" et naviguez jusqu'au fichier .vmx

**N.B.** pour passer à la suite VMwarePlayer doit être lancé une fois

## Configurez le réseau

__NAT__ est un bonne solution sur un LAN perso, mais si si vous êtes dans un environnement un peu plus contraignant avec des baux DHCP, il peut être utile et plus sécurisé ;), de ne pas avoir à enregistrer les adresses MAC de chaque machine avec une IP locale.

__Host-Only__ peut être utile pour créer un réseau privé, mais vos VMs n'auront pas accès à Internet ...

En installant VMPlayer 2 réseaux virtuels fort utiles ont été créés  __vmnet1__ et __vmnet8__
Dans la section network de votre  vm > Sélectionnez "Bridged mode" > et "vmnet8"

![Network configuration](/images/seclab/pentestlab-conf-vm.png "Network configuration"){class="img-responsive center-block"}

lancez votre Kali via VirtualBox et votre OWASP BWA via VMPlayer

## Quid des serveurs vulnérables

Vous pouvez installer cette version d'ubuntu server pour par exemple avoir la vulnérabilité Heartbleed par exemple

* [http://ubuntu-release.locaweb.com.br/12.04/](http://ubuntu-release.locaweb.com.br/12.04/)

Ou bien cette vielle wheezy pour pouvoir jouer avec un [local root exploit](http://www.tux-planet.fr/local-root-exploit-pour-les-noyaux-linux-2-6-37-a-3-8-10/)

* [Debian 7.0.0 – amd64 – netinst](http://www.debiantutorials.com/download-debian-iso/)

Il y a aussi des applications web vulnérables

* [DVWA](http://www.dvwa.co.uk/)
* [mutillidae](http://sourceforge.net/projects/mutillidae/)

## Enjoy your SecLab

les VMs peuvent se pinger mutuellemùent et ont accès à l'Internet

Vous pouvez par exemple commencer de mapper les VMs de votre réseau virtuel avec un magnifique

~~~language-bash
nmap -sV -O -PN 172.16.76.0/24
~~~

* -sV détecte les versions des services
* -O détecte les versions des OS
* -PN évite le ping et vérifie les ports ouverts

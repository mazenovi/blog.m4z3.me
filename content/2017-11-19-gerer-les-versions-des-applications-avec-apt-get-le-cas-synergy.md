Title: Gérer les versions des applications avec apt-get le cas synergy
Date: 2017-11-19 08:34
Author: mazenovi
Category: bricolage
Tags: open-source, linux
Status: published
Slug: gerer-les-versions-des-applications-avec-apt-get-le-cas-synergy
lang: fr
save_as: gerer-les-versions-des-applications-avec-apt-get-le-cas-synergy.html
url: gerer-les-versions-des-applications-avec-apt-get-le-cas-synergy.html

J'utilise quotidiennement [Synergy](https://doc.ubuntu-fr.org/quicksynergy) pour partager mes claviers / souris / buffer entre mon fix et mon portable.
Les deux systèmes étant sous linux-mint j'utilise synergy (gratuitement) via les packet debian: seuls les bianires étant payant, c'est le monde ubuntu qui compile pour moi et me distribue une version up-to-date via mes repos.

Synergy est très sensible aux numéros de versions i.e. si tu as deux versions différentes sur les systèmes avec lesquels tu veux partager tes claviers / souris / buffer, soit ça ne fonctionnera pas, soit tu auras des pertes de fonctionnalités. Dans mon cas j'avais perdu le buffer partagé.

Voici comment remettre les choses d'aplomb

pour avoir les infos détaillés de tes paquets

```bash
sudo apt-cache show synergy
```

sur mon laptop ça donne

<pre class="output">
Package: synergy
Priority: optional
Section: universe/net
Installed-Size: 3589
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Original-Maintainer: Jeff Licquia <licquia@debian.org>
Architecture: amd64
Version: 1.6.2-0ubuntu2
Depends: libavahi-compat-libdnssd1 (>= 0.6.16), libc6 (>= 2.14), libcrypto++9v5, libgcc1 (>= 1:4.1.1), libqt4-network (>= 4:4.5.3), libqtcore4 (>= 4:4.8.0), libqtgui4 (>= 4:4.8.0), libstdc++6 (>= 5.2), libx11-6 (>= 2:1.2.99.901), libxext6, libxi6 (>= 2:1.2.99.4), libxinerama1, libxtst6
Filename: pool/universe/s/synergy/synergy_1.6.2-0ubuntu2_amd64.deb
Size: 770522
MD5sum: 2b35350f6a81c20bf20fbff39b2e52cc
SHA1: 0d7872561aac7f4c1b5565f6fe834c916ae4afa0
SHA256: e5cfc7d1496ce5ab64eeccdaf61b7b733934f5a7f9988f5693adbe101843854b
Description-fr: Partage de souris, clavier et presse-papiers sur le réseau
 Synergy permet de partager facilement une souris et un clavier entre
 plusieurs ordinateurs avec différents systèmes d'exploitation, chacun avec
 son propre affichage, sans matériel spécial. Il est destiné aux
 utilisateurs ayant plusieurs ordinateurs sur leur bureau avec chacun
 utilisant son propre affichage.
 .
 Rediriger la souris et le clavier est aussi simple que de déplacer la
 souris en dehors des limites de l'écran. Synergy regroupe aussi les
 presse-papiers de tous les systèmes en un seul, permettant ainsi le
 couper/coller entre les systèmes. De plus, il synchronise les économiseurs
 d'écran pour qu'ils démarrent et s'arrêtent en même temps. Si le
 verrouillage d'écran est activé, seul un écran nécessite un mot de passe
 pour tous les déverrouiller.
 .
 Les paquets pour Windows, MacOS et RPM ainsi que les sources sont
 disponibles à http://sourceforge.net/project/showfiles.php?group_id=59275
Description-md5: 5fade0f66a7ce7fd1077db2409b0fd30
Homepage: http://synergy-foss.org/
Bugs: https://bugs.launchpad.net/ubuntu/+filebug
Origin: Ubuntu
</pre>

Alors que sur le fixe ça donne

<pre class="output">
Package: synergy
Version: 1.8.8-1~getdeb1
Architecture: amd64
Maintainer: GetDeb Package Ninjas <package.ninjas@getdeb.net>
Installed-Size: 4035
Depends: libavahi-compat-libdnssd1 (>= 0.6.16), libc6 (>= 2.14), libcurl3 (>= 7.16.2), libgcc1 (>= 1:3.0), libqt4-network (>= 4:4.5.3), libqtcore4 (>= 4:4.8.0), libqtgui4 (>= 4:4.8.0), libssl1.0.0 (>= 1.0.0), libstdc++6 (>= 5.2), libx11-6 (>= 2:1.2.99.901), libxext6, libxi6 (>= 2:1.2.99.4), libxinerama1, libxtst6
Homepage: http://synergy-project.org/
Priority: optional
Section: x11
Filename: pool/apps/s/synergy/synergy_1.8.8-1~getdeb1_amd64.deb
Size: 875370
SHA256: a27774dc4b7fa4a16ebcde8f64807ea1e3776413a5056478a058775ab9f8429c
SHA1: 7b637f59626793c6dc2d4c95e08cc63e245f7f77
MD5sum: 6d7083f68dda69c1d865b1c197cdb3bc
Description: Share mouse, keyboard and clipboard over the network
 Synergy lets you easily share a single mouse and keyboard between
 multiple computers with different operating systems, each with its
 own display, without special hardware.  It's intended for users
 with multiple computers on their desk since each system uses its
 own display.
 .
 Redirecting the mouse and keyboard is as simple as moving the mouse
 off the edge of your screen.  Synergy also merges the clipboards of
 all the systems into one, allowing cut-and-paste between systems.
 Furthermore, it synchronizes screen savers so they all start and stop
 together and, if screen locking is enabled, only one screen requires
 a password to unlock them all.
Description-md5: 5284835c0478aa0d3ac563f1caa434da
Original-Maintainer: Jeff Licquia <licquia@debian.org>

Package: synergy
Priority: optional
Section: universe/net
Installed-Size: 3589
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Original-Maintainer: Jeff Licquia <licquia@debian.org>
Architecture: amd64
Version: 1.6.2-0ubuntu2
Depends: libavahi-compat-libdnssd1 (>= 0.6.16), libc6 (>= 2.14), libcrypto++9v5, libgcc1 (>= 1:4.1.1), libqt4-network (>= 4:4.5.3), libqtcore4 (>= 4:4.8.0), libqtgui4 (>= 4:4.8.0), libstdc++6 (>= 5.2), libx11-6 (>= 2:1.2.99.901), libxext6, libxi6 (>= 2:1.2.99.4), libxinerama1, libxtst6
Filename: pool/universe/s/synergy/synergy_1.6.2-0ubuntu2_amd64.deb
Size: 770522
MD5sum: 2b35350f6a81c20bf20fbff39b2e52cc
SHA1: 0d7872561aac7f4c1b5565f6fe834c916ae4afa0
SHA256: e5cfc7d1496ce5ab64eeccdaf61b7b733934f5a7f9988f5693adbe101843854b
Description-fr: Partage de souris, clavier et presse-papiers sur le réseau
 Synergy permet de partager facilement une souris et un clavier entre
 plusieurs ordinateurs avec différents systèmes d'exploitation, chacun avec
 son propre affichage, sans matériel spécial. Il est destiné aux
 utilisateurs ayant plusieurs ordinateurs sur leur bureau avec chacun
 utilisant son propre affichage.
 .
 Rediriger la souris et le clavier est aussi simple que de déplacer la
 souris en dehors des limites de l'écran. Synergy regroupe aussi les
 presse-papiers de tous les systèmes en un seul, permettant ainsi le
 couper/coller entre les systèmes. De plus, il synchronise les économiseurs
 d'écran pour qu'ils démarrent et s'arrêtent en même temps. Si le
 verrouillage d'écran est activé, seul un écran nécessite un mot de passe
 pour tous les déverrouiller.
 .
 Les paquets pour Windows, MacOS et RPM ainsi que les sources sont
 disponibles à http://sourceforge.net/project/showfiles.php?group_id=59275
Description-md5: 5fade0f66a7ce7fd1077db2409b0fd30
Homepage: http://synergy-foss.org/
Bugs: https://bugs.launchpad.net/ubuntu/+filebug
Origin: Ubuntu
</pre>

Pour voir ce que le système va installer

```bash
sudo apt-cache policy synergy
```

<pre class="output">
synergy:
  Installé : 1.8.8-1~getdeb1
  Candidat : 1.8.8-1~getdeb1
 Table de version :
 *** 1.8.8-1~getdeb1 500
        500 http://archive.getdeb.net/ubuntu xenial-getdeb/apps amd64 Packages
        100 /var/lib/dpkg/status
     1.6.2-0ubuntu2 500
        500 http://archive.ubuntu.com/ubuntu xenial/universe amd64 Packages
</pre>

Je vois ici que j'ai deux versions possible de synergy, l'une dans le repo ```xenial-getdeb/apps``` , l'autre dans ```xenial/universe```, chacune a la même priorité (500): c'est la meilleure version qui gagne.

on peut forcer la l'installe

```bash
sudo apt-get install synergy=1.6.2-0ubuntu2 500
```

Mais le problème se reposera au prochain ```apt-get upgrade```.

Pour régler définitivement le problème il y a plusieurs approches:

- sur mon fixe dans ```/etc/apt/preferences``` ajouter

```bash
Package: synergy
Pin: origin "xenial/universe"  
Pin-Priority: 1001  
```

  augmentera le score du repo ```xenial/universe``` pour le paquet synergy ([Assign highest priority to my local repository](https://askubuntu.com/questions/135339/assign-highest-priority-to-my-local-repository)).


- ajouter le repo ```xenial-getdeb/apps``` sur mon laptop

```bash
wget -q -O - http://archive.getdeb.net/getdeb-archive.key | sudo apt-key add -
sudo sh -c 'echo "deb http://archive.getdeb.net/ubuntu xenial-getdeb apps" >> /etc/apt/sources.list.d/getdeb.list'
sudo apt update && sudo apt upgrade
```

La deuxième solution me permet d'avoir un version plus récente sur mes deux machines c'est donc celle que je privilégie.

Title: configurer un site django avec ispconfig
Date: 2018-01-12 08:34
Author: mazenovi
Category: developpement
Tags: open-source, linux
Status: published
Slug: configurer-un-site-django-avec-ispconfig


Je me suis un peu fait mal à la tête pour déployer un site django sur ispconfig3.

Voici mes notes ;)

Ici on part du principe que le path du vhost est /var/clients/client0/web1 ou /var/www/mysite.com (qui est lien symbolique du précédent).

Pour que vous puissiez écrire dans les paths que j'ai utilisé simplement (i.e. sans avoir à faire des ```chattr -i``` dans tous les sens) rendez-vous dans l'interface d'admin de votre instance d'ispconfig3.

```
System > Server Config > votre instance > Web > Permissions > Make web folders immutable (extended attributes) > décochez et sauvez
```

![no-chattr.png](/images/ispconfig/no-chattr.png)

(j'enlève aussi tous les préfixes dans System Config > Sites mais ç a ce n'est pas obligatoire )

## Dépendences

Il y a de fortes chances pour que vous souhaitiez utiliser MySQL avec Django.

Dans mon cas j'utilise un ldap pour l'authentification.

Il y a quelques prérequis pour que les modules python concernés par ces deux services fonctionnent correctement.

### avec python3

```
$ sudo apt-get install python3-dev libmysqlclient-dev mysql-server mysql-server # pour mysql
$ sudo apt-get install -y python-dev libldap2-dev libsasl2-dev libssl-dev # pour ldap
```

### avec python2

```
$ sudo apt-get install python-dev libmysqlclient-dev mysql-server mysql-server # pour mysql
$ sudo apt-get install -y python-dev libldap2-dev libsasl2-dev libssl-dev # pour ldap
```

## Installer wsgi

wsgi et mod_python ne fonctionnent pas ensemble: il vous faudra en premier lieu, vérifier que mod_python est bien désactivé avant d'installer mod_wsgi

* désactiver mod_python

```
$ sudo a2dismod python
```
Deuxième chose à savoir mod_wsgi est compilé pour l'une ou l'autre version de Python (2 ou 3). Il y a donc deux modules possibles.

* installer mod_wsgi (pour python 2)

```
$ sudo apt-get install libapache2-mod-wsgi
```

* installer mod_wsgi (pour python 3)

```
$ sudo apt-get install libapache2-mod-wsgi-py3
```

## Créer et utiliser un virtualenv

* installer virtualenv et pip

```
apt-get install python-setuptools
easy_install pip
pip install virtualenv
```

* création et activation du venv

```
cd /var/www/mysite.com/
sudo mkdir venv
sudo chown web1:client0
virtualenv venv
. /venv/bin/activate
```

N.B il est possible de spécifier la version de pytgon à utiliser dans le virtualenv

```
virtualenv venv -p /usr/bin/python2.6
```

#### Installer les paquets utiles

* pour python2 avec MySQL

```
pip install MySQL-python
```
* pour python3 avec MySQL

```
pip install mysqlclient
```
* le reste

```
pip install django
pip install PIL
...
```

## Configurer Apache pour éviter la [UnicodeEncodeError](https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/modwsgi/)

* éditer /etc/default/locale comme suit
```
export LANG='en_US.UTF-8'
export LC_ALL='en_US.UTF-8'
```
* dans /etc/apache2/envvars décommenter la ligne 28
```
## Uncomment the following line to use the system default locale instead:
. /etc/default/locale
```


## Configurer le Apache pour éviter la [UnicodeEncodeError](https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/modwsgi/)

```
Alias /media/ /var/www/mysite.com/limos/limos/media/
Alias /static/ /var/www/mysite.com/limos/limos/static/

Alias /robots.txt /var/www/mysite.com/limos/limos/static/robots.txt
Alias /favicon.ico /var/www/mysite.com/limos/limos/static/images/favicon.ico

WSGIDaemonProcess mysite.com user=web1 group=client0 python-home=/var/www/mysite.com/venv python-path=/var/www/mysite.com:/var/www/mysite.com/venv/lib/python2.7/site-packages
WSGIProcessGroup mysite.com
WSGIScriptAlias / /var/www/mysite.com/limos/limos/wsgi.py

<Directory /var/www/mysite.com/limos/>
  Require all granted
</Directory>

<Directory /var/www/mysite.com/limos/>
  <Files wsgi.py>
    Require all granted
  </Files>
</Directory>
```    

Notez aussi qu'on utilise **WSGIDaemonProcess** qui est le seul moyen d'obtenir un virtualenv par vitual host (ce qui est indispensable si vous projetez d'héberger plusieurs django sur votre ispconfig)

### Vérifier au fur et à mesure ce qui pose problème

* au niveau python

```
python manage.py check
```

* au niveau apache

```
vi log/errors.log
```

## Liens

http://devmartin.com/blog/2015/02/how-to-deploy-a-python3-wsgi-application-with-apache2-and-debian/

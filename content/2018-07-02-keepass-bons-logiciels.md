Title: Keepass utilisez les bons logiciels
Date: 2018-07-02 00:00
Author: mazenovi
Category: sécurité
Tags: www, android, linux, windows, open-source
Slug: keepass-bons-logiciels
Status: published

## TL;DR

### Pour mon PC (Linux, MacOS, Windows)

* J'utilise [KeePassXC](https://keepassxc.org/download/) comme gestionnaire de mots de passe.
* J'installe l'extension [KeePassXC-browser](https://github.com/keepassxreboot/keepassxc-browser) pour [FireFox](https://addons.mozilla.org/en-US/firefox/addon/keepassxc-browser/) et pour [chrome](https://chrome.google.com/webstore/detail/keepassxc-browser/oboonakemofpalcgghocfoadofidjkkk).


### Pour mon Smartphone (Android)

* J'utilise [Keepass2Android](https://github.com/PhilippC/keepass2android)
* J'installe [KeyboardSwap for Keepass2Android](https://play.google.com/store/apps/details?id=keepass2android.plugin.keyboardswap2) pour l'intégration (cette partie est réservée aux plus technophiles)

### Pour synchroniser ma base de données de mot de passe KeePass (.kdbx)

* J'utilise un [owncloud](https://owncloud.org/) maîtrisé, mais ça marche aussi avec DropBox ou tout autre service de fichiers dans les nuages

# Version Longue

## Le problème avec les mots de passe ...

C'est qu'on en a plein et

- on est toujours en train de les saisir
- on sait qu'on ne doit pas utiliser le même partout ce qui demande un gros effort de mémoire
- on sait que les noter "en clair" dans un calepin ou dans un fichier Excel est dangereux

C'est pourquoi il existe des logiciels que l'on nomme des gestionnaires de mots de passe.

Les gestionnaires de mot de passe ne datent pas d'hier et n'apportent finalement qu'un peu d'ergonomie et d'automatisme dans un processus que l'on pourrait gérer "manuellement".

En effet si vous savez chiffrer votre fichier Excel de mot de passe, avec disons PGP, vous réalisez exactement l'opération qu'un gestionnaire de mots de passe est sensée réaliser.

Le gestionnaire de mots de passe vous permet de ne pas retenir les commandes à exécuter pour chiffrer / déchiffrer et de ne pas vous préoccupper de bien supprimer la version en clair après utilisation par exemple ... ce qui est bien pratique et sécurisant.

Le nerf de la guerre avec le gestionnaire de mots de passe c'est la synchronisation de vos périphériques: comment avoir ses mots de passe sur son PC et son téléphone? Son PC de bureau et son PC perso?

Pour réaliser cette synchronisation il faut un service de synchronisation de fichiers du genre [Dropbox](https://dropbox.com) ou mieux un cloud auquel seul vous pouvez avoir accès (type [ownCloud](https://owncloud.com)).

Mais bien sûr il y a encore plus pratique. Il y a le service de gestion de mot de passe en ligne : [LastPass](https://www.lastpass.com/fr), [Dashlane](https://www.dashlane.com/), [iCloud](https://www.icloud.com/) ou même [Google passwords](https://passwords.google.com) par exemple.

Sans entrer dans les détails, laisser ses mots de passe en clair à un service c'est prendre deux risques:

- que le service les utilise à votre place: même s'il stocke vos mots de passe de manière chiffrée, il y a toujours un moment où il peut intercepter votre secret. Evidemment il ne va pas publier des posts facebook foireux à votre insu. Mais il pourrait très bien explorer vos profils sur différents services. Je ne dis ni qu'il est légal de le faire, ni qu'un des services cités va le faire : c'est un risque.

- que le service se fasse voler les mots de passe: ce scénario n'est pas à exclure. De plus il peut être parfaitement silencieux.

La bonne pratique est donc de garder le secret permettant d'ouvrir le gestionnaire de mots de passe hors de portée du service de synchronisation de fichiers.

## Le problème avec le web c'est ...

C'est qu'on a plein de compte en ligne, donc plein de mot de passe à taper dans son navigateur et on en a vite marre de faire des aller retour entre le gestionnaire de mots de passe et son navigateur pour faire des copier / coller.

## ... Les navigateurs

Et attendez!? il y a un gestionnaire de mot de passe dans mon navigateur: pourquoi j'utiliserais un gestionnaire de mots de passe externe, alors que mon navigateur gère ça très bien pour moi?

* FireFox par défaut stocke vos mots de passe en clair dans votre profil. Il est possible de les chiffrer avec un "mot de passe maître", mais cette [solution est loin d'être inviolable](https://www.raymond.cc/blog/how-to-find-hidden-passwords-in-firefox/).

* Google Chrome utilise par défaut, le service en ligne [Google passwords](https://passwords.google.com). Ce service permet de synchroniser les mots de passe entre toutes vos instances de chromes (PC Bureau, PC Perso, Smartphone etc ...).

N.B. [FireFoxSync](https://www.mozilla.org/fr/firefox/features/sync/) permet exactement la même fonctionnalité en changeant de propriétaire (Fondation Mozilla, plutôt que Google: et que ce soit clair, je  ne discute même pas pour savoir qui est le plus éthique. Je ne veux faire confiance ni à l'un, ni à l'autre). (*Note pour plus tard: Essayer de synchroniser ses mots de passe avec FireFoxSync en ayant un mot de passe maître dans FireFox*)

Ce qu'il faut retenir c'est que, par défaut, les navigateurs ne prennent pas soin de vos mots de passe.

En revanche laisser son navigateur gérer ses mots de passe est très confortable pour l'utilisateur.

Dans la suite il s'agit d'approcher le plus possible ce confort en gardant la main sur ses mots de passe de manière sécurisée.

## Keepass Family

Dans la famille des gestionnaires de mots de passe souverains (c'est à dire installés sur votre machine et pas sur un serveur que vous ne maîtrisez pas) il est un grand nom de longue date: KeePass (since 2003).

* Keepass2 dans sa version portable fait parti des [Produits Certifiés CSPN par l'ANSSI](https://www.ssi.gouv.fr/entreprise/certification_cspn/keepass-version-2-10-portable/) ce qui est une bonne garantie.
    * Pour info il est également une recommandation officielle au CNRS par exemple.

Keepass2 possède pas mal de fonctionnalités intéressantes qui le rende agréable à utiliser.

On peut notamment ajouter des fichiers à ses entrées de mots de passe.

N.B. les formats de base de données de mots de passe gérer par Keepass 1 (.kdb) et 2 (.kdbx) sont incompatibles. Il est toutefois possible de convertir un kdb en kdbx avec Keepass2.

Parmi les fonctionnalités de keepass2, l'[auto-type](https://keepass.info/help/base/autotype.html), qui permet de lancer la saisie automatique des nom d'utilisateur / mot de passe à partir de KeePass, est très appréciée.

Mais le système n'est pas encore parfait ... il faut ouvrir les liens à partir de KeePass pour que les formulaires d'authentification se remplissent automatiquement .

### Intégration au navigateur

Keepass 2 offre également un grand choix de [plugins](https://keepass.info/plugins.html), dont un particulièrement crucial: [KeePass HTTP](https://keepass.info/plugins.html#keepasshttp).

L'idée de ce plugin est de rendre la base de données de mot de passe gérable (en lecture et en écriture) via HTTP, il sera alors possible de faire dialoguer le navigateur avec le gestionnaire de mot de passe, et donc de gérer la base de données de mots de passe à partir du navigateur et non plus exclusivement à partir du gestionnaire de mot de passe.

Concrètement le navigateur sera capable de présenter les entrées du gestionnaire de mots de passe associées à l'url courante consultée par l'utilisateur (si tant est que la base de données de mot de basse soit déverrouillée et qu'une telle entrée existe). Le navigateur pourra aussi enregistrer de nouveaux mots de passe dans la base de données de mots de passe en cliquant sur "enregistrer ce mot de passe".

[KeeFox](https://www.kee.pm/) est la première extension à sortir exploitant cette fonctionnalité.
Suivront [PassIFox](https://addons.mozilla.org/en-US/firefox/addon/passifox/) et [chromeIPass](https://chrome.google.com/webstore/detail/chromeipass/ompiailgknfdndiefoaoiligalphfdae) qui implémentent la même fonctionnalité.

### KeePass pour tous

Le problème majeur de KeePass c'est qu'il est écrit pour le monde Windows. Il n'y a pas de portage pour linux, BSD ou Mac. Le seul moyen d'utiliser KeePass sur ces sytèmes est d'installer [mono](https://doc.ubuntu-fr.org/mono) qui embarque 1Go de lib Microsoft à elle toute seule :/ Ca ne fait pas très envie.

Un portage mutli-plateforme apparaît quelques années après la sortie de KeePass: [KeePassX](https://www.keepassx.org/).
Le projet prend un peu d'ampleur mais se pose bientôt la question de la fonctionnalité de la communication entre le gestionnaire de mot de passe et le navigateur. En d'autres termes pouvoir utiliser [KeeFox](https://www.kee.pm/) (ou [PassIFox](https://addons.mozilla.org/en-US/firefox/addon/passifox/) ou [chromeIPass](https://chrome.google.com/webstore/detail/chromeipass/ompiailgknfdndiefoaoiligalphfdae)) avec [KeePassX](https://www.keepassx.org/).

[KeePassX](https://www.keepassx.org/) fait le choix de privilégier la sécurité à l'ergonomie et décide de ne pas intégrer la fonctionnalité ([la pull request est encore ouverte](https://github.com/keepassx/keepassx/pull/111)) qui, dans sa version HTTP, est réputée vulnérable.

Après plus d'un an d'attente et pas de réaction de la part de [KeePassX](https://www.keepassx.org/), le projet est finalement forké en [KeePassXC](https://keepassxc.org/): [KeePassX](https://www.keepassx.org/), mais avec le plugin de communication avec le navigateur via HTTP en plus!

Et devinez quoi? depuis mars dernier la [communication entre le gestionnaire de mots de passe et le navigateur a été sécurisé en passant non plus par HTTP mais par des sockets ;) - ce qui est plus fiable](https://github.com/varjolintu/keepassxc-browser/issues/28)!

### Et mon smartphone?

Alors je ne me prononcerai pas pour le monde Mac (que je connais mal), mais pour le monde Android j'utilise [Keepass2Android](https://github.com/PhilippC/keepass2android). Il gère nativement un large éventail de connecteurs permettant de synchroniser le gestionnaire de mots de passe de différentes façon (dont [ownCloud](https://owncloud.com)).

![KeePass2Android connncetor](images/keepass/keepas2android.connect.png){class="img-responsive center-block" width="50%"}

### Et l'intgération au navigateur sur mon smartphone?

Il y a [moyen](https://github.com/PhilippC/keepass2android/blob/master/docs/Advanced-usage-of-the-Keepass2Android-keyboard.md) avec [KeyboardSwap for Keepass2Android](https://play.google.com/store/apps/details?id=keepass2android.plugin.keyboardswap2) mais c'est pour les bricoleurs.

En effet il faudra passer par [adb](https://developer.android.com/studio/command-line/adb) pour modifier manuellement l'option (les explications complètes sont sur la [page PlayStore de l'app](https://play.google.com/store/apps/details?id=keepass2android.plugin.keyboardswap2))

```
$ pm grant keepass2android.plugin.keyboardswap2 android.permission.WRITE_SECURE_SETTINGS
```

Pour utiliser l'intégration [Keepass2Android](https://github.com/PhilippC/keepass2android) au navigateur il vous faudra cliquer sur le lien à ouvrir à partir de [Keepass2Android](https://github.com/PhilippC/keepass2android) et, automatiquement un clavier virtuel spécial s'ouvrira.

Vous pourrez alors renseigner vos identifiants en 2 clics.

![KeePass2Android virtual keyboard](images/keepass/keepas2android.keyboard.png){class="img-responsive center-block" width="50%"}

## Conclusion

A date la bonne équation semble être

* sur PC: [KeePassXC](https://keepassxc.org/) + [KeePassXC-Browser Migration](https://keepassxc.org/docs/keepassxc-browser-migration/)
* sur Anroid : [Keepass2Android](https://github.com/PhilippC/keepass2android) + [KeyboardSwap for Keepass2Android](https://play.google.com/store/apps/details?id=keepass2android.plugin.keyboardswap)

Je pense sincèrement que [KeePassXC](https://keepassxc.org/) va prendre de plus en plus d'importance: il est multi plateforme et a résolu avec élégance et sécurité le problème de l'intégration au navigateur qui est une vraie attente des utilisateurs.

Sur Android il est encore possible d'améliorer l'intégration en simplifiant la mise en place du clavier virtuel, reste à savoir si c'est [Keepass2Android](https://github.com/PhilippC/keepass2android) qui soignera ce point en premier.

Plus généralement cette tranche de vie d'un projet open-source est typique: la dualité sécurité VS ergonomie est un vrai sujet qui pose une question fondamentale concernant les logiciels de sécurité: l'objectif est il de protéger le mieux possible (on privilégie l'aspect sécurité) ou de protéger le plus grand nombre (on privilégie alors l'ergonomie pour faciliter l'adoption)?

Title: Sécuriser Wordpress
Date: 2015-01-29 14:23
Author: mazenovi
Category: sécurité
Tags: open-source, www, linux
Slug: securiser-wordpress
Status: published


## Qui héberge des wordpress et ne s'est jamais fait p0wned?

### Menteurs!

Perso ça m'est arrivé plus d'une fois et j'avoue que je ne 'métais pas intéressé au dossier de prêt jusqu'à ce que je migre tous les blgos que j'héberge et que je me décide du coup à durcir un peu ma politique de sécurité concernant WP.

Tout d'abord je me suis un peu attardé sur [wp-cli](http://wp-cli.org/fr/) qui est un assez chouette outil pour réaliser des opérations sur plusieurs blogs à la fois, notamment pour les mises à jour qui sont un peu le nerf de la guerre.

Je me suis ensuite rendu compte que depuis WordPress 3.7 les mises à jours mineures étaient automatiquement passées par WordPress ...

cool! Mais ni les mises à jours majeures, ni les mises à jour de plugins ... du coup j'ajoute systématiquement les lignes suivantes dans le fichier **functions.php de mon thème** (plutôt que dans le wp-config.php comme c'est suggéré [ici](http://codex.wordpress.org/Configuring_Automatic_Background_Updates), car ça ne fonctionnait pas)

```php
add_filter( 'auto_update_plugin', '__return_true' );
```

active la mise à jour automatique des plugins

```php
add_filter( 'auto_update_theme', '__return_true' );
```

active la mise à jour automatique des thèmes

```php
add_filter( 'auto_update_translation', '__return_true' );
```

active la mise à jour automatique des traductions

```php
add_filter( 'allow_minor_auto_core_updates', '__return_true' );
```

active spécifiquement les mises à jour mineures , utilisez la commande suivante

```php
add_filter( 'allow_major_auto_core_updates', '__return_true' );
```

active spécifiquement les mises à jour majeures

```php
add_filter( 'automatic_updates_is_vcs_checkout', '__return_false', 1 );
```

active spécifiquement les mises à jour automatiques, même si un dossier de versionning (.git, .hg, .svn etc...) a été trouvé dans le répertoire WordPress ou un de ses sous répertoire.

Bien sûr tout cela nécessite de faire des thèmes propres et des sauvegardes régulières.

**N.B** Ca ne marchera pas avec les thèmes enfants

Ensuite pour durcir un peu le tout je vérife au niveau du **wp-config.php**

```php
define('FORCE_SSL_ADMIN', true);
```

vérifie que la partie administration n'est accessible que via https comme c'est indiqué ici

```php
define('DISALLOW_FILE_EDIT', true);
```

que les fichiers php ne sont pas éditables en ligne comme c'est indiqué ici Pour finir j'ajoute en suite dans le .htaccess en dehors du # BEGIN WordPress et # END WordPress,.

les lignes suivantes

RewriteCond %{REQUEST_FILENAME} wp-content/.+\.(php|txt|pl)$ [NC]

protègent le dossier wp-content - les modules php ne devraient pas être accédés directement (la sécurité repose sur une "bonne pratique" qui devrait être appliquée par le développeur du plugin

```bash
RewriteCond %{REQUEST_FILENAME} !cookies-for-comments/css\.php$ [NC]
```

exception pour "Cookies for Comments" qui utilise un fichier CSS avec du PHP [cc lang='bash' ]

```bash
RewriteRule .* - [F,NS,L]
RewriteCond %{REQUEST_FILENAME} wp-includes/.+\.php$ [NC]
```

protège le dossier wp-includes - ne permet pas l'accès direct aux fichiers php

```bash
RewriteCond %{REQUEST_FILENAME} !wp-includes/ms-files\.php$ [NC]
```

Permet d'uploader des fichiers via multi-site

```bash
RewriteCond %{REQUEST_FILENAME} !wp-includes/js/tinymce/.+ [NC]
```

tinymce a des modules php, on autorise donc ces modules spécifiques

```bash
RewriteRule .* - [F,NS,L]
```

Toujours dans l'.htacess on peut empêcher l'accès direct au fichier wp-config.php comme c'est suggéré ici

```bash
order allow,deny deny from all
```

Il est également conseillé de

* virer tous les plugins inutiles
* ne pas utiliser de plugin permettant l'exécution de code PHP ou demandant l'accès aux fichiers de conf
* faire des sauvergades régulières

Ce ne sont que les mesures que j'ai retenu vous êtes bien entendu invités à lire l'intégralité des recommandations.

Ici trois pistes à explorer

* http://www.ossec.net/ pour détecter les intrusions
* https://www.modsecurity.org/ pour détourner les requêtes malveillantes (à manipuler avec précautions)
* &lt;troll>arrêter d'utiliser wordpress&lt;/troll>

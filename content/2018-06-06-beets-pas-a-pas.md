Title: beets pas à pas
Date: 2018-06-06 11:00
Author: mazenovi
Category: musique
Tags: open-source, download
Slug: beets-pas-a-pas
Status: published


## Chercher dans sa discothèque

c'est ```ls``` qui permet de lister votre discothèque par mots clés

```bash
$ beet ls "fauvisme"
```

Par défaut tous les titres, albums ou artistes contenant le motif recherchés vont être listés.

Il est possible de rechercher le motif dans un champs particulier.

```bash
$ beet ls "albumartist:noir"
```

notez ici que le motif est insensible à la casse (i.e. il liste les artistes contenant Noir,noir,nOir,NOIR etc ...)

Pour rendre le motif sensible à la casse il suffit d'utiliser ```::``` plutôt que ```:```

```bash
$ beet ls "albumartist:noir"
```

ici les "Tétines Noires" ne seront pas listés à cause du N majuscule ...

### Affichage par album

Avec la commande précédente seuls les titres des artistes contenant le motif "noir" sont listés.

Si ce sont effectivement les noms d'artistes qu'on cherche, faire afficher tous les titres de chacun de leurs albums n'est pas très lisible

ici l'option ```-a``` va permettre de lister les albums plutôt que tous les titres qu'ils contiennent

```bash
$ beet ls -a "albumartist:noir"
```

### Affichage du path

Si vous cherchez dans votre discothèque pour copier de la musique sur votre téléphone par exemple. Vous serez probablement intéressés par l'option ```-p``` qui affiche le path du dossier de l'album plutôt que simplement le nom de l'artiste et le nom de l'album

```bash
$ beet ls -a "albumartist:noir"
```

### Déplacer des albums

```
beet modify -a "albumartist:ahmad" "album:Live" albumartist="Ahmad Jamal"
```

* doit bouger les fichiers

### Supprimer des albums

* les recherches vont peut être faire apparaître des doublons, un artiste tagué une fois avec une majuscule et une fois sans par exemple,

```
beet remove "albumartist:capercaillie"
```

Vous pouvez ajouter

* -a pour l'album

* -d pour effectivement supprimer

l'utilisation ```::``` est recommandé pour éviter les fausses manip

### Plus de méta

l'option ```-f``` permet de personnaliser les méta données affichées par ls

```
beet ls -f '$album: $format $bitrate' "album:Stories From the City, Stories From the Sea"
```

faire afficher le format et le bitrate aidera probablement en cas de doublon pour savoir lequel gardé.

## Explorer la base de données de sa bibliothèque

### interroger la librairie en SQL avec sqlite3

en allant interroger directement la base de données de beets on apprend des chose intérssantes et on peut faire des manip assez indispensable.
J'utilise [sqlite3](https://www.sqlite.org/download.html) en ligne de commande sous ubuntu mais n'importe quel client sqlite peut faire l'affaire.

#### ouvrir la bases de données

```
sqlite3 /media/mazenovi/DataMaze/MUSIC/beets/library.blb
```

#### explorer les tables

```
sqlite> .table;
```

#### explorer les champs d'une table

```
sqlite>  PRAGMA table_info(albums);
```

N.B. tout ces champs peuvent être utilisé dasn les requêtes avec ls et dans l'affichage avec ```-f```.

#### déplacer sa bibbliothèque

il se peut, si votre bibliothèque a un peu vécu, que certains paths d'album ne soient plus à jour

##### liste les différents paths

```
sqlite> select distinct(substr(path,0,29)) from items;
```

le paramètres 29 est à ajusté avec la longueur du path le plus petit utilisé avec votre BDD.

Dans mon cas

```
/media/mazenovi/MUSIC/music/
/media/mazenovi/DataMaze/MUS
```

Je sais que le path ```/media/mazenovi/MUSIC/music``` n'existe plus. Je vais donc le mettre à jour avec le nouveau path ```/media/mazenovi/DataMaze/MUSIC/music/```

```
sqlite> select count() from items where path like '/media/mazenovi/MUSIC/music/%';
```

donnera le nombre de path à réécrire

```
sqlite> select path from items where path not like '/media/mazenovi/MUSIC/music/%';
```

affiche les paths à jour

```
sqlite> select "/media/mazenovi/DataMaze/MUSIC/music/" || substr(path, 29) from items where path like '/media/mazenovi/MUSIC/music/%';
```

permet de vérifier qu'on a bien identifié la partie à accoler au nouveau path. C'est le bon moment pour faire une sauvegarde de ```/media/mazenovi/DataMaze/MUSIC/beets/library.blb```

```
sqlite> update items set path = "/media/mazenovi/DataMaze/MUSIC/music/" || substr(path, 29) where path like '/media/mazenovi/MUSIC/music/%';
sqlite> update albums set artpath = "/media/mazenovi/DataMaze/MUSIC/music/" || substr(artpath, 29) where artpath like '/media/mazenovi/MUSIC/music/%';
```

peut prendre un petit temps, et met effectivement à jour les paths des morceaux et des cover pour les albums

```
sqlite> select distinct(substr(path,0,29)) from items;
```

n'affiche plus qu'un path: le bon!

## Modifier des albums

Pour que les modif puissent se faire sur le système de fichier il est indispensable que les paths soient valides (c.f. section précédente)

voici un exemple:
```
beet ls -a "albumartist:chicks on speed"
```
donne

```
Chicks on Speed - 99 Cents
Chicks on Speed - Chix 52
Chicks on speed - Will Save Us All!
Chicks on Speed - Wordy Rappinghood
Chicks on Speed and The No Heads - Press the Spacebar
```

*Chicks on speed - Will Save Us All!* fait que j'ai deux dossiers pour le même artist: l'un nommé *Chicks on Speed* et l'autre *Chicks on speed*.

```
beet modify -a "album::Will Save Us All!" albumartist="Chicks on Speed"
```

permet de régler cela

## Supprimer des albums

```
beet remove -da "album:Paul’s Boutique"
```

l'option -d est indispensable pour supprimer le dossier sur au nievau du système de fichier, sinon la suppression ne se fait que dans la base de données de beets.

## Quitter sqlite3

```
sqlite> .quit
```

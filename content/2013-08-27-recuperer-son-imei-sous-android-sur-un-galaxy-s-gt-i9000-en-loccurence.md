Title: Récupérer son IMEI sous android sur un galaxy s GT i9000
Date: 2013-08-27 09:23
Author: mazenovi
Category: bricolage
Tags: android
Slug: recuperer-son-imei-sous-android-sur-un-galaxy-s-gt-i9000-en-loccurence
Status: published

Après avoir essayé pas mal de ROM sur mon Galaxy S je me suis aperçu, au
moment de demander le désimlock à l'opérateur, que mon IMEI était
inconsistant.

Le numéro IMEI est grosso modo aux smartphones ce que l'adresse MAC est
aux cartes réseaux.

On peut le faire afficher en tapant \*\#06\# dans le téléphone, mais il
se trouve aussi sur la boîte du téléphone et / ou dans le téléphone sous
la batterie.

Un numéro IMEI inconsistent ressemble à un truc du genre 0499XXXXX.

Tout ce qui concerne le numéro IMEI est rangé dans le répertoire /efs du
système. [Galaxy S
Unlock](https://play.google.com/store/apps/details?id=com.helroz.galaxysunlock&hl=fr)
ou
[GalaxSimUnlock](https://play.google.com/store/apps/details?id=com.spocky.galaxsimunlock&hl=fr)
permettent d'effectuer une sauvegarde de ce répertoire (sur un système
rooté) mais bien entendu on découvre en général ce qu'est un IMEI et les
loigiciels pour le sauvegarder au moment où on l'a perdu, après avoir
flashé  les ROMS les plus exotiques comme un gros bourin ...

On lit pas mal de truc pas rassurant quand on a perdu son IMEI et c'est
finalement [J²](https://twitter.com/EauMaltHoublon) qui m'a mis sur la
piste. J'ai supprimé deux fichiers (nv\_data.bin et
nv\_data.bin.md5) dans le répertoire /efs (avec la ligne de commande,
via un explorer genre [ES
Explorer](https://play.google.com/store/apps/details?id=com.estrongs.android.pop&hl=fr),
ou via ssh avec
[SSHDroid](https://play.google.com/store/apps/details?id=berserker.android.apps.sshdroid)
par exemple).

Les deux fichiers ont été recréés au démarrage suivant et j'ai récupéré
mon IMEI :)

quelques notes supplémentaires

-   Faire un hard reset (effacer TOUT les dossiers des programmes /
    préférences / données utilisateurs) : \*2767\*3855\#
-   [Flasher la dernière stock ROM (I9000XXJVU
    Android 2.3.6)](http://galaxys-team.fr/viewtopic.php?f=18&t=22119&hilit=jvu+firmware) et
    la rooter
-   [Android 4.2 (ou 4.1) avec Cyanogen Mod
    10.1](http://galaxys-team.fr/viewtopic.php?f=6&t=27003)
-   [Cyanogen pour Galaxy
    S](http://wiki.cyanogenmod.org/w/Galaxysmtd_Info)

 

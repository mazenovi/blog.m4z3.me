Title: configurer un site django avec ispconfig
Date: 2018-01-12 08:34
Author: mazenovi
Category: vie-privee
Tags: open-source, linux
Status: draft
Slug: install-brave-on-linux-mint-from-repo

## TLDR;

```
curl https://s3-us-west-2.amazonaws.com/brave-apt/keys.asc | sudo apt-key add -
UBUNTU_CODENAME=`cat /etc/os-release | grep UBUNTU_CODENAME= | cut -d '=' -f 2`
echo "deb [arch=amd64] https://s3-us-west-2.amazonaws.com/brave-apt $UBUNTU_CODENAME main" | sudo tee -a /etc/apt/sources.list.d/brave-$UBUNTU_CODENAME.list
sudo apt update
sudo apt install brave
```


## Meet Brave

Do you know Brave?

If you are concerned about privacy ... probably you should
It make me blog in english ... see how enthusiast I am!

[Brave want to fix the web](https://www.brave.com/how-to-fix-the-web/)! I think we need it now

## So why a post to install brave on mysqlclient

I began to install brave via snap. Snap is currently disabled in linuxmint. I know why now. Default path to download file or to upload bookmark are very strange. I had some issues with snap version of keepassxc so I tried from repo

I like linuxmint for years now, and [Linux install instructions](https://github.com/brave/browser-laptop/blob/master/docs/linuxInstall.md) depend on ubuntu version. Problem: command lines provided by Brave documentation work for ubuntu only.

Title: mod_wsgi sur xampp
Date: 2017-11-20 08:34
Author: mazenovi
Category: developpement
Tags: open-source, linux
Status: published
Slug: mod_wsgi sur xampp


cet article est largement inspiré de [https://community.apachefriends.org/f/viewtopic.php?t=42975](https://community.apachefriends.org/f/viewtopic.php?t=42975)

Dans mon cas j'utiliserai le binaire python de mon système

## activer libtool de xampp

```
sudo vi  /opt/lampp/build/libtool
```

remplacer la première ligne

```
#! /bin/sh
```

par

```
#! /bin/bash
```

## installer mod_wsgi sous XAMPP:

```
cd /tmp
wget https://pypi.python.org/packages/aa/43/f851abaad631aee69206e29cebf9f8bf0ddb9c22dbd6e583f1f8f44e6d43/mod_wsgi-4.5.20.tar.gz
tar xvzf mod_wsgi-4.5.20.tar.gz -C /tmp
cd mod_wsgi-4.5.20/
./configure --prefix=/opt/lampp/ --with-apxs=/opt/lampp/bin/apxs --with-python=/usr/bin/python
make
sudo make install
```

## Ajouter à /opt/lampp/etc/httpd.conf

```
LoadModule wsgi_module modules/mod_wsgi.so
Include etc/extra/httpd-wsgi.conf
```


## Créer le fichier /opt/lampp/etc/extra/httpd-wsgi.conf

```
WSGISocketPrefix /opt/lampp/var/run/wsgi
WSGIScriptAlias  /wsgi-test/    /opt/lampp/var/wsgi/scripts-test/
<Directory /opt/lampp/var/wsgi/scripts-test>
    WSGIApplicationGroup scripts-test
    Require all granted
</Directory>
```

## Créer le script python à servire /opt/lampp/var/wsgi/scripts-test/helloworld.wsgi:

```
#!/usr/bin/python
def application(environ, start_response):
  status = '200 OK'
  output = 'Hello World!'

  response_headers = [('Content-type', 'text/plain'),
                      ('Content-Length', str(len(output)))]
  start_response(status, response_headers)

  return [output]
```


## Redémarrer Apache

```
sudo /opt/lampp/lampp restart
```


ouvrir [http://localhost/wsgi-test/helloworld.wsgi](http://localhost/wsgi-test/helloworld.wsgi) dans son navigateur préféré.

```
hello world!
```

devrait s'afficher

Title: Gérer ses fichiers audio avec beets
Date: 2016-05-18 00:00
Author: mazenovi
Category: musique
Tags: open-source, download
Slug: gere-ses-fichiers-audio-avec-beets
Status: published


J'aime bien deux choses:

* résoudre des problèmes fondamentaux autour d'une bière  avec des copains
* raconter comment vous pouvez résoudre ce problème fondamental sur ce blog

C'est en discutant avec [@madmatah](https://twitter.com/madmatah) que j'ai résolu un problème majeur

## Comment bien tagger ses fichiers audio?

Tagger correctement ses fichiers audio est quelque chose qui préoccupe tous les audiophiles.

* avoir les bons noms d'artistes et album
* avoir un album complet (pas de pistes manquantes ou en surplus)
* avoir l'année de sortie et non l'année de réédition (kikoo deezer qui m'explique que [Surrealistic pillow](http://www.deezer.com/album/77496) date de 2002)
* avoir la jaquette qui va avec le disque
* avoir des tags de genre à peu près cohérents
* avoir des formats audio homogènes (mp3, flac et éviter les m4a et autres wma ...) ou  a minima choisis

Sont autant de petits détails qui valorisent ta collection (entre audiophile le tutoiement est de circonstance) de fichiers audio en la rendant plus agérable à consulter et plus consistante.

L'exerice de tagging des fichiers audio est en général un parcours initiatique qui se déroule à peu près comme ça:

* un jour que tu extrais un CD de ta collection sur ton PC tu découvres que ton "extracteur" audio est capable d'interroger un service (en général [Freedb](https://en.wikipedia.org/wiki/Freedb),  historiquement [CDDB](https://en.wikipedia.org/wiki/CDDB)) pour récupérer la liste des morceaux (tracklist) de ton CD. L'idée est de calculer la signature du disque et d'interroger la base de données avec cette signature. En général cette découverte rend heureux et donne envie de se faire une discothèque "propre", ce qui amène à l'étape suivante ...

* tu as déjà pas mal de fichiers audio et beaucoup s'appellent track01.mp3, ou un nom de fichier correct mais pas de tags ou l'inverse et tu ne les identifies que parce que tu les as mis dans un dossier nommé "Nom de l'artiste - nom de l'album" ... et là tu as envie que ces fichiers soient aussi "propres" que ceux que tu viens d'extraire plus haut. tu découvres alors un nouveau monde, le monde des taggers audio ... il y en a plein qui fonctionnent de manières plus ou moins intuitives et surtout de manières plus ou moins automatisées, on parle alors d'**auto tagging**.

l'**auto tagging** c'est plutôt cool ... si tu t'es déjà tapé quelques giga à tagger à la main tu vois ce que je veux dire

Historiquement j'utilisais [mp3tag](http://www.mp3tag.de/en/) car il est très efficace pour exploiter les sources suivantes: MusicBrainz, discogs, amazon, et d'autres que j'utilisais assez peu ... comme il est windows only j'ai d'abord basculé sur son équivalent libre [puddletag](http://docs.puddletag.net/) qui est un remplaçant digne sous linux ...

Mais aujourd'hui [@madmatah](https://twitter.com/madmatah) m'a fait découvrir [beets](http://beets.io/) et ma vie a changé. Je veux partager ça avec toi!!

## beets

* **Amis windozien** je vais faire mal à tes yeux: beets est un programme **python** (aïe) et tu vas devoir [installer ce langage](https://www.python.org/downloads/) pour espérer l'utiliser. En plus c'est un outil sans interface grahique, tu devras l'utiliser dans ton prompt moisi et ce sera vraiment moche.

  [La marche à suivre est là](http://beets.readthedocs.io/en/v1.3.17/guides/main.html#installing-on-windows).

  Tu peux essayer d'[installer bash sur ton machin](http://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/) ça arrangera peut être les choses.

  Mais sérieusement, mon  ami, après le coup de la [mise à jour forcée vers windows 10](http://www.ginjfo.com/actualites/logiciels/windows-10/windows-10-petition-enquete-mise-a-jour-forcee-20160607) , la [politique de vie privée délétère](http://www.nextinpact.com/news/95988-windows-10-et-vie-privee-options-a-ne-pas-oublier.htm) ... Ne crois tu pas qu'il serait temps d'installer un [vrai système d'exploitation sur ta machine](https://www.linuxmint.com/)?

* **Amis linuxiens** ascendant debian ou ubuntu ou autre (tu sauras convertir je le sais), toi tu [ouvres juste un terminal](https://doc.ubuntu-fr.org/terminal) et tu tapes

  ```bash
  sudo apt-get -y install python-pip
  sudo pip install beets
  ```

  et hop!

### L'autotagging avec beets

La première chose que tu vas vouloir faire c'est importer ta discothèque en l'état. Je présuppose que tu as un dossier par album. beets ne pourra rien pour toi si tu as 3000 mp3 dans un dossier.

Toute la configuration est centralisée dasn un fichier ~/.config/beets/config.yaml. Je recommande cette configuration minimale pour débuter

```yaml
# ~/.config/beets/config.yaml
# python -W ignore ./beet import /media/mazenovi/MazeWallet/Swap -l /media/mazenovi/MazeWallet/beets/$(date +"%m-%d-%Y-%H-%M-%S").log

directory: ~/Musique # le dossier où sera stockée la musique triée par beet

library: ~/.config/beets/musique.blb # la base de donnéees qui stocke toutes les métadonnées des fichiers

import:
    move: yes # beets déplace les albums triés (de là où ils sont aux dossiers mentionnés plus haut)

replace: # ici tous les motifs trouvables dans les métadonnées à transformer pour qu'ils soient compatibles avec un nom de fichier ou de dossier
    '[\\/]': _
#   '^\.': _ j'aim bien voir Abraham Inc. (et pas Abraham Inc_ !!)
#    '[\x00-\x1f]': _
    '[<>:"\?\*\|]': _
    '\.$': _
    '\s+$': ''
    '^\s+': ''

plugins: embedart the lastgenre duplicates missing info discogs # activation de quelques plugins natifs

paths:
    default: %the{$albumartist}/[$year] $album/$track - $title # le motif permettant de nommer les dossiers associés aux albums

the:
    patterns: ['^les ', '^le ', '^la ', '^un '] # les mots à virer. Ne pas oublier l'espace ou "Lady Saw" devient "Dy Saw, La" et on ne la reconnait plus

lastgenre:
    count: 5 #nombre de catégories à récupérer au max

```

pour que ça marche il te fault les librairies suivantes

```yaml
sudo pip install requests pylast discogs-client
```

**N.B. <i class="fa fa-hand-o-right" aria-hidden="true"></i>** il te faudra un compte [discogs](https://www.discogs.com), lors du premier lancement, beets t'enverra vers une page te permettant d'obtenir un token qui te permettra de l'utiliser. T'inquiète il n y'a qu'à se laisser guider.

Si discogs envoie des erreurs concernant le SSL tu peux régler ça en tapotant

```bash
sudo pip install ndg-httpsclient
sudo apt-get install python-pyasn1
```

#### Lancer l'importation

Afin d'avoir des fichiers de [logging](http://beets.readthedocs.io/en/latest/reference/config.html#log) tu peux utiliser l'option -l.

La commande suivante

```bash
beet import /path/to/Music/2import -l ~/.config/beets/$(date +"%m-%d-%Y-%H-%M-%S").log
```

permet donc de garder une trace des dossiers ignorer pour y revenir après

Avec la configuration beets sus mentionée, beets va

* chercher l'album dans musicbrainz, puis dans discogs
* déplacer les fichiers du répertoire (*/path/to/Music/2import*) à importer vers le répertoire **directory** (*~/Musique*)
* récupérer les genres associés à l'album
* faire un répertoire par artiste contenant un répertoire par album avec le schéma de nommage qu'on lui a spécifié dans la conf
  * si un nom d'artiste possède un article (le, la, les, ...) l'article est mis à la fin du nom (ce qui soulage un peu le classement alphabétique)
  * chaque nom d'album est préfixé avec son année de sortie (ça permet d'avoir les albums de chaque artiste dans l'ordre chronologique)

### Répondre aux questions

Lors de l'import les cas suivants peuvent se présenter à chaque répertoire (ou chaque album c'est équivalent tu l'as compris)

* beets a trouvé et il est sûr de son coup: il t'affiche le dossier qu'il traite et il fait ce qu'il y a à faire (tagging + déplacement)

* beets a trouvé un truc qui colle mais n'est pas tout à fait sûr de son coup il veut que tu confirmes: il te permet de prévisualiser les changements sur tes fichiers et te propose de valider direct (souvent il a un bon feeling) ou d'approfondir

* beets a trouvé plusieurs trucs qui pourraient coller et il s'en remet à toi: si tu valides avec entrée tu iras à l'aperçu du premier résultat (le plus pertinent selon beets) et tu pourras prévisualiser les changements, puis valider, ou tu peux approfondir

### Approfondir?

Il y a pas mal de raisons pour que beets ne trouve rien qui matche ou trouve des trucs qui n'ont rien à voir avec la choucroute. Dans ces cas là voilà comment je procède

* [S]kip si je n'arrive pas à trouver de résultats cohérents sur une requête manuelle ou que je sais que l'album en question est bizarre, genre un live qui n'a jamais était édité, je le laisse où il est, et comme il sera mentionné dans les logs je le traiterai plus tard

* s[E]arch est une option qui peut se révèler payante, entrez le nom de l'artiste et un mot clé de l'album et vous pourrez peut être trouver ce que beets n'a pas deviné. En pratique il est préférable d'utiliser l'option [I] combiné à un moteur de recherche

* [I]d consiste à fournir un identifiant de release musicbrainz pour tagguer le répertoire en cours. le meilleur moyen d'obtenir cet identifiant est de rechercher sur https://musicbrainz.org ou avec google genre ['Nico Chelsea girl' musicbrainz](https://www.google.fr/?ion=1&espv=2#safe=off&q=nico+chelsea+girl+musicbrainz).

l'Id de la release se trouve alors en bout d'url
```bash
https://musicbrainz.org/release/096e1175-1001-4a4a-9add-31655aa0b221
-> Id = 096e1175-1001-4a4a-9add-31655aa0b221
```
Beets vous proposera un aperçu des modifications avant d'agir.

**N.B. <i class="fa fa-hand-o-right" aria-hidden="true"></i>** j'utilise assez souvent cette méthode, du coup j'ai écrit une extension chrome qui permet de copier l'id dans le clipboard en 1 clic: ça s'appelle [beets-id](https://github.com/mazenovi/beets-id)

* [U]se as-is permet d'importer les fichiers dans l'état où ils sont, c'est typiquement l'option que vous prendrez si les disques n'existent ni dans discogs, ni dans musicbrainz mais que vous les avez méticuleusement tagués manuellement

* [T]racks permet d'importer chaque piste comme un album (je n'utilise jamais cette option à vrai dire)

* [G]roup albums permet d'importer par albums ou par artistes si un répertoire contient plusieurs albums mélangés

* a[B]ort selon la taille de votre discothèque et surtout selon la rareté de son contenu l'opération peut prendre assez longtemps, n'hésitez pas à interrompre ... beets reprendra là où il s'est arrêté et si vous prenez garde à mettre la date dans vos fichiers de logs vos sessions de taggings sont incrémentales de fait.

A noter que beets mentione également les **missing tracks** et **unmatched tracks**.

* <i class="fa fa-hand-o-right" aria-hidden="true"></i> Des **missing tracks** ne signifient pas nécessairement qu'un album est incomplet, peut être faut il choisir la bonne release (genre la version remasterisée qui contient des pistes bonus).

* <i class="fa fa-hand-o-right" aria-hidden="true"></i> Pour les **unmatched tracks** c'est pareil. Si ce sont vraiment des fichiers en trop, en appliquant les modif, si votre conf déplace les fichiers (c'est le cas de celle que j'ai mentionné plus tôt), ces fichiers ne seront pas copiés. Ne laissant que des albums intègres dans votre discothèque triée.

<!-- * j'ai écrit un tout petit plugin qui ajoute deux options pour déplacer les incomplets, et les inclassables dasn deux dossiers distincts. L'idée est qu'une fois l'importation finie on peut supprimer le répertoire plutôt que d'avoir à revenir sur son contenu en consultant les logs -->

### Activer amazon dans l'autotagger

Ici on est au niveau expert / magie noir

* Tout d'abord il te faudra un compte [Amazon Web service AWS](https://aws.amazon.com/fr/console/)
* Ensuite tu pourras crééer ton access Keys (Access Key ID and Secret Access Key)
* Il te faudra ensuite coupler ton compte à un compte [Amazon Associates Account](https://affiliate-program.amazon.com/signup)
* et pour utiliser la version FR il faudra aussi affilier ce compte [là](https://partenaires.amazon.fr/gp/associates/join/landing/main.html)
* je te laisse le soin de décliner si tu veux utiliser amazon.de .it .jp etc ...

**N.B. <i class="fa fa-hand-o-right" aria-hidden="true"></i>** Si tu ne t'affilies pas ca ne marchera pas et tu auras des erreurs 400 bad request alors fais y bien

Mais c'est pas fini il faut maintenant installer [beet-amazon](https://github.com/jmwatte/beet-amazon) il faut mettre ce fichier [amazon.py](https://raw.githubusercontent.com/jmwatte/beet-amazon/master/amazon.py) et ce [fichier renommé en bottlenose.py](https://raw.githubusercontent.com/lionheart/bottlenose/master/bottlenose/api.py) (issu de [bottlenose](https://github.com/lionheart/bottlenose)) dans un répertoire ~/.config/beets/plugins par exemple. Il faut ensuite référencer ce dossier dans ~/.config/beets/config.yaml et y configurer le plugin amazon

```yaml

pluginpath:
    - ~/.config/beets/plugins

plugins: fetchart embedart the lastgenre duplicates missing info discogs drop amazon

...

amazon:
    source_weight: 1
    Access_Key_ID: XXXXXXXXXXXXXXXX
    Secret_Access_Key: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    preferred_regions: ["FR", "US"]
    asso_tag: beets

```

**N.B. <i class="fa fa-hand-o-right" aria-hidden="true"></i>** le paramètre source_weight n'est pas documenté mais il permet de prioriser un peu plus les résultats amazon. sans une valeur un peu forte tu risques de ne pas les voir même quand ils sont pertinents.

## Ayé t'es prêt

Maintenant tu possède l'arme la plus absolue pour normaliser ta collec' de fichiers audio, et l'utiliser un serveur home theater du genre [kodi](https://kodi.tv/) pourquoi pas :)

![kodi](/images/posts/2016-10-18/kodi.png)

Note mon ami, pour finir, que les albums intaggables sont les plus précieux, car ceux qui sont appelés à disparaître ... on compte sur toi!!

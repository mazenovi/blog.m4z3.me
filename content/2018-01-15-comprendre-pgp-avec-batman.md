Title: comprendre PGP avec Batman
Date: 2018-01-15 13:37
Author: mazenovi
Category: vie-privee
Tags: crypto, gpg
Status: draft

## Vie privée et communication

Trop souvent on pense que l'on a rien à cacher et / ou que nous ne sommes pas des personnes assez *intéressantes* pour être surveillée ou simplement écoutée.

Les révélations de Snowden et les fuites que l'on voit remonter régulièrement indiquent pourtant que la surveillance de masse est une pratique largement répandue dans nos démocraties.

Dans les régimes totalitaires il n'y a pas à argumenter pour comprendre l'enjeu de la protetction des communications pour un activiste ou un journaliste par exemple.

Outre les écoutes étatiques, nos communications sont épeluchées par les services qui les rendent possibles: gmail et autres gratuité, dont le business model est clairement l'exploitation de nos données.

Il existe des services chiffrées, qui font débat, tel que signal ou whatsapp: le problème est ici un non contrôle ou tout du moins un contrôle de faible sur la bonne foi de ces services, quant à leur visu sur nos données.

L'espionnage des communications à des fins criminelles est également à considérer. Il peut avoir des formes assez variées de l'ingénieurie sociale, au chantage, en passant par le vol d'identité bancaire.

Enfin on peut tout simplement faire valoir son droit à la vie privée, et à la liberté de penser.

> "If privacy is outlawed, only outlaws will have privacy.""

> "Si avoir une vie privée devient hors-la-loi, alors seuls les hors-la-loi auront une vie privée"

C'est le constat sentencieux de Phil Zimmermann, auteur de PGP en 1991.

PGP est un outil assez largement connu quand on se préoccupe de maîtriser la sécurité de ses échanges numériques.

GPG est le logiciel cournat qui permet d'utiliser et de gérer ses clés PGP

Il est toutefois assez compliqué à prendre en main et s'il est bien documenté il est parfois difficile de transposer les fonctionnalités présentées aux cas d'utilisation de la vie courante.

## le cas de Batman

Aussi je vous propose un cas d'étude : [Batman](https://fr.wikipedia.org/wiki/Batman)

Batman est un super héros masqué et anonyme il est hors de question que les habitants de Gotham City, la ville où il combat le crime, sachent qu'il est Bruce Wayne le milliardaire.

Il doit donc cacher son identité.

S'il veut être efficace, c'est à dire arriver rapidement sur les lieux du crime avant que celui ci ait lieu, il doit pouvoir être contacté rapidement par tout habitant de Gotham. Il est également crucial que tout habitant soit sûr de parler à Batman et pas à un hurluberlu qui se prend pour super héros en herbe. Ce problème est connu sous le terme d'**authentification** de la source du message

Il doit être certain que le message qu'on lui envoie n'a pas été modifié. Imaginons qu'une vielle dame crois le Joker dans les rues de Gotham. Elle sort discrètement son portable pour envoyer un message à Batman: "J'ai vu le Joker à l'angle de Penny Street. J'ai peur qu'il prépare un sale coup". Le joker pourrait très bien tenter d'intercepter le message, en écoutant le traffic du relais téléphonique le plus proche par exemple, et modifier le message comme suit: "J'ai vu le Joker au Lincoln. J'ai peur qu'il prépare un sale coup" et relayé le sms avec l'identité de la vieille dame. Ce problème est connu sous le terme d'**intégrité** du message

Enfin s'il veut rester discret sur ces interventions, les messages qu'on lui envoie et ses réponses doivent restés confidentielles. Ce problème est connu sous le terme de **confidentialités** des messages

## PGP

PGP vise à garantir ces 3 points

* **authentification**
* **intégrité**
* **confidentialités**

les 2 premiers points garantissent de fait la **non répudiation**: c'est à dire que si la vieille dame a écrit : "Je me fais agresser par le pingouin" et que son seul but est de voir Batman parce qu'elle le trouve sexy ... elle ne pourra pas dire que ce n'est pas elle qui l'a dit.

## Chiffrement Symétrique

Par chiffrer on entend rendre un message (ou un fichier) illisible à tout personne non autorisée à le déchiffrer, c'est à dire le lire en clair.

le chiffrement symétrique permet d'effectuer cette opération en produisant un message illisble conditionné par un secret. ce même secret permet de transformer le message illisible (la version chiffrée) en le message original.

Le chiffrement pose un problème insolvable: comment faire transiter le secret entre l'émetteur du message et le destinatire de manière sécurisée.

## Chiffrement Asymétrique

Le chiffrement asymétrique, ou encore à clé pulique, résout ce problème.

Plutôt que de partager un mot de passe le destinataire va posséder une "identité" en deux parties sous la forme d'un trousseau composée d'une paire de clé, l'une qui est connue par la terre entière ou au moins par quiconque souhaite lui écrire: on l'appelle la **clé publique**, l'autre qui n'est connue que de lui seul et protégée par un mot de passe: on l'appelle la **clé privée**.

Ces deux clés fonctionnent de manière conjointe et symétrique.

* Tout ce que l'on chiffre avec la clé publique est déchiffrable avec la clé privée.

* Tout ce que l'on chiffre avec la clé privée est déchiffrable avec la clé publique.

### Chiffrement

Dans le cas de Batman et de la vieille dame voici comment cela se passe:

1. Batman a publié sa clé publique sur son site web, et sur des flyers qui ont été largement distribué à Gotham City
2. La vieille dame a importé cette clé publique dans son téléphone au cas où elle voit quelque chose de suspect ou elle se fasse agressée
3. Quand elle voit le Joker elle écrit un message à Batman en le chiffrant avec la clé publique de Batman
4. Le message est transmis à Batman et n'est lisible par personne d'autre que lui
5. Batman recoit le message chiffré, le déchiffre avec sa clé privée (cette opération nécessite la saisie de son mot de passe) et peut lire le message d'alerte de la vielle dame
6. Si la vielle dame possède elle même un trousseau composé d'une paire de clés et que Batman connait la clé publique de la vieille dame (elle lui à mis en pièce jointe de son message par exemple), il va pouvoir lui répondre de manière chiffrée en chiffrant avec cette clé publique: "OK j'arrive tout de suite"
7. La vieille dame pourra alors déchiffrer la réponse avec sa propre clé privée (en tapant son mot)

C'est le processus d'échange de message chiffré en chiffrement asymétrique

### Signature

Il y a un problème là dedans c'est que le message chiffré ne contient aucune information sur l'expéditeur. Ce problème peut paraître bizarre car on pourrait penser qu'il suffit de regarder l'adresse de l'expéditeur du message pour le savoir.

Pour comprendre pourquoi on ne peut pas faire confiance à l'adresse de l'expéditeur, considérons ce cas où aucune signature n'est utilisée

1. La vielle dame a prévenu Batman et Batman a répondu comme expliqué plus haut
2. Le Joker se retourne voit la vieille dame en train de lire le message de Batman et lui arrache son téléphone des mains
3. Il répond au dernier message de Batman qui dit qu'il arrive tout de suite par un message du genre : "Ah non je me suis trompé c'était un jeune qui s'habille bizarre. Excusez moi pour le dérangement"
4. Si Batman recoit ce message le Joker a gagné

La **signature** résout ce problème: signer un message c'est chiffrer le message que l'on envoie (ou plutot une signature recalculable de ce message - c'est plus léger) avec sa clé privée et le joindre au message chiffrée avec la clé publique de son destinataire.

en ayant ces 2 messages le destinataire peut

1. déchiffrer le message qui a été chiffré avec sa clé publique grâce à sa clé privée (il doit saisir son mot de passe pour cette opération)
2. chiffrer le message qu'il vient de déchiffrer en 1 avec la clé publique de son expéditeur
3. comparer la signature qu'il a reçu en pièce jointe du message et le message chiffré qu'il a obtenu en 2: si les deux chiffrés sont identiques le destinataire est alors sûr que c'est bien l'expéditeur est possesseur de la clé publique qu'il sait être la sienne

Reprenons le cas de Batman et de la vielle dame avec un message signé afin d'être sûr de bien comprendre le problème qui est résolu

1. La vielle dame a prévenu Batman par un message chiffré qu'elle a pris la peine de signer et Batman a répondu comme expliqué plus haut
2. Le Joker se retourne voit la vieille dame en train de lire le message de Batman et lui arrache son téléphone des mains
3. Il répond au dernier message de Batman qui dit qu'il arrive tout de suite par un message du genre : "Ah non je me suis trompé c'était un jeune qui s'habille bizarre. Excusez moi pour le dérangement"
4. Pour berner batman le Joker doit signer le message avec la clé privé de la vielle dame. Sinon ce sera louche puisque le premier

message était signé. Et pour ça le Joker doit connaître le mot de passe associée à la clé privée de la vieille dame: il est chocolat

La signature garantit que celui qui écrit est bien le possesseur de la clé publique associé à la clé privée avec laquelle il a réalisé cette signature.

[Dans le prochain post nous verrons comment Batman utilise gpg au quotidien]()

## La toile de confiance

Ce que PGP cherche à établir c'est une confiance de trousseau de paires de clés à trousseau de paires de clés. En réalisant ça il garantit la confiance quelques soit le moyen de transmission: mail, sms, chat, mais aussi twitter, statut facebook, commentaires de blog si ca vous amuse, les messages chiffrés et les signatures ne craignent théoriquement pas l'exposition.

Si on cherche à garantir sa sécurité on évitera tout de fois de les exposer inutilement bien entendu.

Cette confiance pose un autre problème celui de la confiance initiale que l'on peut avoir dans une clé ...

[Sur le sujet de la confiance et de la gestion des clés, Batman a pas mal réfléchi et je vous propose d'y consacrer le prochain]

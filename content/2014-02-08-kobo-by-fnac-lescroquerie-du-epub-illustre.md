Title: Kobo by Fnac L'<strike>escroquerie</strike> la mésaventure du epub illustré
Date: 2014-02-08 14:23
Author: mazenovi
Category: blog
Tags: www
Slug: kobo-by-fnac-lescroquerie-du-epub-illustre
Status: published

![Kobo 0](images/kobo-0.png){: align="right" style="margin: 15px"}

Il y a un an et demi ma chérie m'a offert un [kobo glo](http://fr.kobo.com/koboglo).

Pour la Saint Valentin elle décide de m'acheter une [BD sur le vin](http://www4.fnac.com/SearchResult/ResultList.aspx?SCat=0!1&Search=chronique+de+la+vigne&sft=1&submitbtn=OK)
et voyant qu'elle est [disponible sur Kobo](http://livre.fnac.com/a5843282/Fred-Bernard-Chroniques-de-la-vigne-conversations-avec-mon-grand-pere?NUMERICAL=Y#FORMAT=Epub%20Illustr%C3%A9) elle décide de me l'acheter au format numérique.

Vu que j'aime la BD, le vin et lire des trucs sur mon **Kobo** c'est une
triple bonne idée ... et pourtant!

Elle n'est pas spécialement technophile mais elle connaît la différence
entre un [**Kindle**](www.amazon.fr/kindle) (**format mobi**) et un
**Kobo**. Elle sait que le format du **Kobo** c'est le **epub** ...

Donc elle vérifie si ce livre est bien un **epub** et elle lit **epub illustré**
ce qui est, avouons-le, assez engageant quand on possède un
appareil qui lit les **epub**.

![Kobo 1](images/kobo-1.png)

Mais ma chérie à un mari geek et elle sait combien l'informatique peut
être compliqué donc elle scrolle un peu plus bas dans la page et voit
l'encart  **"Indispensable avec chronique de la vigne, conversations
avec mon grand-père" qui a l'air de présenter les matériels à posséder
pour lire l'achat qu'elle s'apprête à faire**.

![Kobo 2](images/kobo-2.png)

Elle clique donc en toute logique sur le lien "En savoir plus" et se
voit afficher une page qui présente fièrement les **Kobo Glo**, celui la
même qui est sur ma table de chevet.

![Kobo 3](images/kobo-3.png)

Elle valide l'achat sereine.

Elle appelle ensuite la hotline pour savoir si elle pourrait récupérer
le fichier sous un format quelconque afin d'en faire quelque chose
d'offrable (je pense qu'elle pensait à une clé USB ou un truc du genre -
bon là j'avoue je lui ai pas bien expliqué les DRM) et en discutant avec
la hotline elle se rend compte que ce qu'elle vient d'acheter ne pourra
pas se lire sur un **Kobo Glo** (c'est à ce moment que j'ai pris
l'histoire en route).

-   Sur un **téléphone: oui** (super la lecture d'une BD sur un Galaxy
    même S3!!)
-   sur un **Mac/PC: oui** (mouai ...)
-   sur une **tablette: oui** (j'en ai pas - impeccable!)
-   mais sur le support pour lequel elle avait décidé de me l'offrir -
    un **Kobo Glo: non**

S'en suit une conversation assez surréaliste avec le service technique
de la fnac qui prétend (et il a sûrement raison) que rien est possible:
pas de remboursement, pas d'échange, rien. Je précise qu'à cet instant
de l'histoire l'achat a été effectué quelques minutes avant et que la BD
qui fait l'objet de l'**achat n'a même pas été téléchargée** (et la
Fnac.com le sait très bien avec son système de DRM).

Seule consolation du service technique : **nous ne sommes pas les seuls
à nous être fait avoir**.

Tu métonnes?!

Il faut bien regarder pour trouver le lien "en savoir plus", **au milieu
de la page, en petit**

![Kobo 4](images/kobo-4.png)
{: style="text-align:center"}

qui nous amène effectivemenet sur un message qui stipule que le **epub
illustré** n'est compatible qu'avec le **KoboArc** et les solutions
logiciel pour **Android et iOS**

![Kobo 5](images/kobo-5.png)
{: style="text-align:center"}

Au final l'information est incomplète en plus car il existe un [Kobo
desktop pour windows](http://fr.kobo.com/desktop)mais qui utilise encore
windows hein ?! certainement pas les clients de la fnac ...

Même une fois cette application découverte on est pas sorti de
l'auberge, parce qu'elle est tellement bien faite, que même avec un 24
pouces et tous les réglages qui vont bien on a du mal à lire ce qu'il se
dit dans les bulles (réalisé sans trucage).

![Kobo 6](images/kobo-6.png)

Mr l'agitateur culturel,

-   [Mon droit de rétractation n'existe
    plus?](http://vosdroits.service-public.fr/particuliers/F10485.xhtml) 
    Je ne suis pas juriste aussi j'aimerais bien savoir dans quelle case
    rentre l'achat de ma chérie pour que vous en soyez dispensé. Mais ni
    vos [CGV](http://www4.fnac.com/Help/fnaccom-cgv.aspx#bl=foot)  

    ![Kobo 7](images/kobo-7.png)

    ni votre page sur le [retour d'un
    produit](http://selfservice.fnac.com/selfservice_FR/template.do?id=8761)
    ne me paraissent claires  

    ![Kobo 8](images/kobo-8.png)

-   Vous êtes un acteur historique de la vente de produits culturels et
    les consommateurs accordent un certain crédit à votre enseigne dont
    vous vous servez à loisir pour leur balancer de la pub ciblée, mais
    assez peu pour les éduquer à votre nouveau business numérique ...
    -   **Kobo by Fnac** ce sont les **liseuses qui lisent ou pas des
        formats epub différents**, les appareills physiques. **Kobo by
        Fnac** c'est aussi le logiciel (qui lit tout les formats
        livres numériques). Vous avez des perles en termes de
        marketing ... Vous pouvez écrire **Kobo by Fnac** lit tous les
        formats et expliquer dans le même temps aux gens qui se sont
        fait avoir que seul l'appareil **Kobo** nouvelle génération lit
        le livre qu'ils viennent d'acheter. C'est très clair ... tout
        est fait pour berner le client et lui expliquer en plus qu'il
        n'a pas bien lu. C'est très vexant, pour ne pas dire malhonnête
    -   De la même manière, pourquoi **epub** et **epub illustré** alors
        que ce sont des formats incompatibles? Vous allez nous faire le
        coup du **epub animé**? du **epub 3D**? Et à chaque fois faut
        tout racheter?
    -   Le pire c'est que vous pourriez très bien mettre en place une
        solution de remplacement pour les **anciens modèles de Kobo**
        dans la mesure où **il est tout à fait possible de lire des BD
        dessus via les formats** [cbr,
        cbz, ...](http://fr.wikipedia.org/wiki/Comic_book_archive) C'est
        trop compliqué? C'est trop cher? Je comprends! Quand on s'est
        fait à l'idée qu'on allait économiser le transport, l'entrepot,
        le magasin, le vendeur, la caissière, on a du mal à se dire
        qu'on va mettre en place une solution pour que le client soit
        satisfait (mais ils se prennent pour qui tous ces gens là?!?)

Vous allez me dire oui mais **Kobo** c'est même nous la Fnac pas qui
nous en occupons, c'est un truc à part. Alors je vous répondrai que Vous
avez bien pris la peine d'y aposer votre signature **Kobo by fnac**.

Et en plus sur leur site à eux chez **Kobo** c'est clair ...

![Kobo 9](images/kobo-9.png)
{: style="text-align:center"}

il ya l'icône tablette et pas l'iĉone liseuse ce qui évite au quidam de
faire des achats inutils qu'il ne peut être ni se faire rembourser, ni
échanger.

Mr l'agitateur culturel vous venez de me convaincre

-   de ne (plus) jamais acheter une BD en ligne!
-   de l'incapacité de vos services marketing à mettre en place une
    offre numérique cohérente et intelligible.
-   de l'incapacité de vos web ergonomes et autres web designers à
    présenter une information claire sur fnac.com pour que le
    consommateur puisse s'y retrouver (les consommateurs sont ici les
    gens qui lisent des livres, donc des gens qui accordent du sens aux
    mots, donc des gens qui quand ils lisent "indispensable avec"
    pensent trouver en dessous les matériels avec lesquels ils peuvent
    lire ce qu'il y a au dessus et non une pub).
-   de l'incapacité de vos services techniques à résoudre un quelconque
    problème autrement qu'en culpabilisant le consommateur ("c'était
    écrit dans les CGV" ... ce qui jusqu'à preuve du contraire est faux)
    ou en lui expliquant qu'il n'est pas le seul à s'être fait avoir
-   que je me suis bien fait berner philosophiquement parlant. Libriste
    convaincu, je suis allé vers un Kobo plutôt qu'un kindle car le
    format epub est un standard ouvert, compatible avec toutes les
    plateformes (windows, mac, android, et même linux) alors que le
    mobipocket est un standard propriétaire. Mais en fait ca revient
    exactement au même dans quand on piège un format libre avec des
    DRM ... ce que je peux être naïf des fois ... pardon? Ah oui! c'est
    pas la fnac c'est Kobo ...

Et pour finir vous avez réussi à gâcher le cadeau de ma chérie, avant la
St valentin!

Salut l'agitateur culturel, et bonne chance dans ta nouvelle vie de
vendeur d'aspirateur

> Edit au 15/02/2014: Après ce post la fnac nous a remboursé l'achat du
> epub illustré dont il est question dans ce post, reconnaissons leur ce
> geste commercial. Elle a également remis de l'ordre dans ces bannières
> publicitaires afin qu'on ne les confonde plus avec des informations
> utiles, ce qui montre sa bonne volonté et sa réactivité.
>
> Soyez vigilant toutefois sur fanc.com et ailleurs:
>
> - sur la compatibilité entre le format du epub (surtout s'il n'est pas
> epub tout court) et le modèle de votre liseuse
>
> - Le remboursement dont nous avons fait l'objet est un geste
> commercial. **Le droit de rétaractation n'existe toujours pas à ma
> connaissance en ce qui concerne les achats multimédia en ligne sur
> fnac.com!**
>
> - Si vous envisagez de lire vos BD avec la solution logiciel de
> l'offre Kobo by Fnac installez là avant sur votre périphérique et
> faites deux ou trois essais avec des BD gratuites (il y en a!) au
> format que vous comptez acheter, ça vous évitera les mauvaises
> surprises
>
> Merci à tout ceux qui ont relayé cet article ...

Title: Connaître son système Linux
Date: 2018-01-01 00:00
Author: mazenovi
Category: privacy
Tags:
Slug: connaitre-son-systeme-linux
Status: draft

sometimes we have to answer this question
* a server we don't use to admin
* an old machine
* an unknown machine ;)

You can access 4 information types with

## system information (including processor type and host name)

uname -a

## Kernel information are available with

cat /etc/*-release

or

cat /proc/version



## distribution information



lsb_release -a



## "comercial" or "human name" information



cat /etc/issue



## CPU

Another good question would be what about CPU?

cat /proc/cpuinfo



grep flags /proc/cpuinfo



or



$ grep -o -w 'lm' /proc/cpuinfo | sort -u




CPU modes are :

* lm flag means Long mode cpu - 64 bit CPU
* Real mode 16 bit CPU
* Protected Mode is 32-bit CPU

## La ram

$ free -t
             total       used       free     shared    buffers     cached
Mem:       1534820     812240     722580          0      52064     433856
-/+ buffers/cache:     326320    1208500
Swap:       489940          0     489940
Total:     2024760     812240    1212520

$ free -m
             total       used       free     shared    buffers     cached
Mem:          1498        799        699          0         50        429
-/+ buffers/cache:        318       1180
Swap:          478          0        478


$ dmesg | grep Memory:
Memory: 1525364k/1563456k available (4673k kernel code, 36668k reserved, 2121k data, 656k init, 654152k highmem)

+htop

http://www.linuxpedia.fr/doku.php/materiel/connaitre_son_materiel

http://www.commentcamarche.net/faq/8781-identifier-son-processeur

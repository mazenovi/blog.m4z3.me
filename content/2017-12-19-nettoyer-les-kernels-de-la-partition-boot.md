Title: nettoyer les kernels inutilisés de la partition /boot dans ubuntu
Date: 2017-12-19 08:34
Author: mazenovi
Category: bricolage
Tags: open-source, linux
Status: published


Il arrive qu'au cours d'un ```apt upgrade``` ubuntu crache ce genre de message

```
update-initramfs: Generating /boot/initrd.img-4.4.0-96-generic

gzip: stdout: No space left on device
E: mkinitramfs failure cpio 141 gzip 1
update-initramfs: failed for /boot/initrd.img-4.4.0-96-generic with 1.
dpkg: error processing package linux-firmware (--configure):
 subprocess installed post-installation script returned error exit status 1
Setting up linux-image-4.4.0-103-generic (4.4.0-103.126) ...
Running depmod.
update-initramfs: deferring update (hook will be called later)
Examining /etc/kernel/postinst.d.
run-parts: executing /etc/kernel/postinst.d/apt-auto-removal
4.4.0-103-generic /boot/vmlinuz-4.4.0-103-generic
run-parts: executing /etc/kernel/postinst.d/initramfs-tools
4.4.0-103-generic /boot/vmlinuz-4.4.0-103-generic
update-initramfs: Generating /boot/initrd.img-4.4.0-103-generic

gzip: stdout: No space left on device
E: mkinitramfs failure cpio 141 gzip 1
update-initramfs: failed for /boot/initrd.img-4.4.0-103-generic with 1.
run-parts: /etc/kernel/postinst.d/initramfs-tools exited with return code 1
Failed to process /etc/kernel/postinst.d at
/var/lib/dpkg/info/linux-image-4.4.0-103-generic.postinst line 1052.
dpkg: error processing package linux-image-4.4.0-103-generic (--configure):
 subprocess installed post-installation script returned error exit status 2
dpkg: dependency problems prevent configuration of
linux-image-extra-4.4.0-103-generic:
 linux-image-extra-4.4.0-103-generic depends on
linux-image-4.4.0-103-generic; however:
  Package linux-image-4.4.0-103-generic is not configured yet.

dpkg: error processing package linux-image-extra-4.4.0-103-generic
(--configure):
 dependency problems - leaving unconfigured
dpkg: dependency problems prevent configuration of linux-image-generic:
 linux-image-generic depends on linux-image-4.4.0-103-generic; however:
  Package linux-image-4.4.0-103-generic is not configured yet.
 linux-image-generic depends on linux-image-extra-4.4.0-103-generic;
however:
  Package linux-image-extra-4.4.0-103-generic is not configured yet.
 linux-image-generic depends on linux-firmware; however:
  Package linux-firmware is not configured yet.

dpkg: error processing package linux-image-generic (--configure):
 dependency problems - leaving unconfigured
Setting up linux-headers-4.4.0-103 (4.4.0-103.126) ...
Setting up linux-headers-4.4.0-103-generic (4.4.0-103.126) ...
Setting up linux-headers-generic (4.4.0.103.108) ...
dpkg: dependency problems prevent configuration of linux-generic:
 linux-generic depends on linux-image-generic (= 4.4.0.103.108); however:
  Package linux-image-generic is not configured yet.

dpkg: error processing package linux-generic (--configure):
 dependency problems - leaving unconfigured
```

Le problème est assez intelligible: ```/boot``` manque de place pour déployer le nouveau kernel (ça se vérifie facilement avec un ```df -h```).

Il va donc falloir supprimer les kernels intuilisés sur la partition ```/boot``` ...

## Connaître le kernel courant du système

```
$ uname -a
Linux control.m4z3.me 4.4.0-96-generic #119-Ubuntu SMP Tue Sep 12 14:59:54 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux
```

le kernel courant est ici le 4.4.0-96

## Lister les kernels présents dans /boot

```
$ dpkg -l | grep linux-image
iF  linux-image-4.4.0-103-generic        4.4.0-103.126                              amd64        Linux kernel image for version 4.4.0 on 64 bit x86 SMP
ii  linux-image-4.4.0-83-generic         4.4.0-83.106                               amd64        Linux kernel image for version 4.4.0 on 64 bit x86 SMP
rc  linux-image-4.4.0-92-generic         4.4.0-92.115                               amd64        Linux kernel image for version 4.4.0 on 64 bit x86 SMP
ii  linux-image-4.4.0-93-generic         4.4.0-93.116                               amd64        Linux kernel image for version 4.4.0 on 64 bit x86 SMP
ii  linux-image-4.4.0-96-generic         4.4.0-96.119                               amd64        Linux kernel image for version 4.4.0 on 64 bit x86 SMP
iU  linux-image-extra-4.4.0-103-generic  4.4.0-103.126                              amd64        Linux kernel extra modules for version 4.4.0 on 64 bit x86 SMP
ii  linux-image-extra-4.4.0-83-generic   4.4.0-83.106                               amd64        Linux kernel extra modules for version 4.4.0 on 64 bit x86 SMP
rc  linux-image-extra-4.4.0-92-generic   4.4.0-92.115                               amd64        Linux kernel extra modules for version 4.4.0 on 64 bit x86 SMP
ii  linux-image-extra-4.4.0-93-generic   4.4.0-93.116                               amd64        Linux kernel extra modules for version 4.4.0 on 64 bit x86 SMP
ii  linux-image-extra-4.4.0-96-generic   4.4.0-96.119                               amd64        Linux kernel extra modules for version 4.4.0 on 64 bit x86 SMP
iU  linux-image-generic    
```

## Supprimer les kernels inutiles dans /boot

Tout kernel avec un numéro de version inférieur à celui du kernel courant peuvent être supprimé

```
sudo apt remove --purge linux-image-4.4.0-83-generic
sudo apt remove --purge linux-image-4.4.0-92-generic
sudo apt remove --purge linux-image-4.4.0-93-generic
```

## Relancer l'upgrade

```
sudo apt upgrade
```

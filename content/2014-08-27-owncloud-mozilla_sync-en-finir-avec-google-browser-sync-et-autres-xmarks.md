Title: owncloud mozilla_sync - en finir avec Google Browser sync et autres XMarks
Date: 2014-08-27 16:09
Author: mazenovi
Category: vie privée
Tags: open-source, linux
Slug: owncloud-mozilla_sync-en-finir-avec-google-browser-sync-et-autres-xmarks
Status: draft

![ie\_ff\_chrome](http://old.mazenod.fr/wp-content/uploads/2014/08/ie_ff_chrome.jpg){.alignleft
width="200" height="150"}

http://forum.owncloud.org/viewtopic.php?f=26&t=21146

https://gist.github.com/mariussturm/061b9f4861ef1292aa60

Vous me direz[GoogleBrowser Sync n'existe
plus](http://www.google.com/tools/firefox/browsersync/) et vous aurez
bien raison! Mais la problématique de la synchronisation des navigateurs
reste entière pour autant: que ce soient la synchro chrome qui donne
toutes vos données de navigation à Google ou des services de type
XMarks. Si on se préoccupe un tant soit peu de la confidentialité de ses
données on ne devrait synchroniser ses données de navigation qu'avec des
outils que l'on maitrîse.

C'est ce que propose le plugin owncloud
[mozilla\_sync](https://github.com/owncloud/mozilla_sync) pour le
navigateur firefox. On pourrait s'arrêter là mais Mozilla nous a réservé
une pépite dont il a le secret ...

Depuis quelques temps FireFox c'est doté d'un service [FireFox
sync](https://support.mozilla.org/fr/kb/comment-configurer-firefox-sync?redirectlocale=fr&redirectslug=firefox-sync-emporter-infos-perso)
qui permet de synchroniser vos marque-pages, votre historique, vos mots
de passe, modules complémentaires et onglets ouverts. Le petit plus est
que ce service **pouvait** être hébergé sur un serveur dédié, jusqu'à
FireFox29 ...

Depuis la 29 quand vous allez dans Edition &gt; Préférences &gt; Sync
les liens vous amènes systématiquement sur le service FirFox Sync
hébergé par Mozilla ce qui vous interdit de configurer votre propre
serveur de synchro ...

[![ff1](http://old.mazenod.fr/wp-content/uploads/2014/08/ff1.png){.aligncenter
.wp-image-2302 width="598"
height="472"}](http://old.mazenod.fr/wp-content/uploads/2014/08/ff1.png)Mais
il y a une feinte, si vous aviez configuré un serveur de synchro perso
avec FireFox 28 par exemple et que vous avez ensuite mis à jour la
synchro continue de fonctionner. Voilà comment il faut procéder pour
paramétrer votre propre serveur de synchro.

Allez à <about:config>, cliquez sur l'avertissement "oui je ferais
attention", faites un clic droit n'importe où dans la fenêtre,
sélectionnez "Nouvelle" &gt; "Chaîne de caractères" et saisissez
**services.sync.username**

**[![ff2](http://old.mazenod.fr/wp-content/uploads/2014/08/ff2.png){.aligncenter
.wp-image-2303 width="581"
height="295"}](http://old.mazenod.fr/wp-content/uploads/2014/08/ff2.png)**

cliquez sur OK et saisissez n'importe quelle chaîne de caractères ici :
**anythingyouwant**

 

[![ff3](http://old.mazenod.fr/wp-content/uploads/2014/08/ff3.png){.aligncenter
.wp-image-2304 width="594"
height="296"}](http://old.mazenod.fr/wp-content/uploads/2014/08/ff3.png)

cliquez sur OK

retournez dans dans Edition &gt; Préférences &gt; Sync et vous devriez
voir apparaître une fenêtre de ce genre qui vous permettra de paramétrer
votre propre serveur sync

[![ff4](http://old.mazenod.fr/wp-content/uploads/2014/08/ff4.png){.aligncenter
.wp-image-2305 width="588"
height="462"}](http://old.mazenod.fr/wp-content/uploads/2014/08/ff4.png)

Alternativement vous pouvez aussi directement ajouter directement cette
ligne à votre pref.js  
\[cc lang='bash' \]  
user\_pref("services.sync.username", "anythingyouwant");  
\[/cc\]

Ensuite rendez vous dans votre la rubrique "Personnel" (en haut à
gauche) de votre owncloud. rubrique "Mozilla Sync" vous trouverez tous
les renseignements dont vous avec besoin pour paramétrer la synchro.

En guise de conclusion, ne tirons pas sur l'ambulance même s'il est
assez maladroit de la part de mozilla de n'offrir sans bricolage qu'une
possibilité de synchro avec ses serveurs, cette situation est
visiblement temporaire le temps que les serveurs FireFox Sync soit
distribués pour que tout un chacun puisse installer le sien ;)

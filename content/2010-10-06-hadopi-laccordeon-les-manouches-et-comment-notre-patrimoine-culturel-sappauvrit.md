Title: Hadopi, l'accordéon, les manouches, et comment notre patrimoine culturel s'appauvrit
Date: 2010-10-06 08:34
Author: mazenovi
Category: blog
Tags: open-source, download, www
Slug: hadopi-laccordeon-les-manouches-et-comment-notre-patrimoine-culturel-sappauvrit
Status: published



![Manouche partie](images/hadopi/joprivat.jpg){: align="left" style="margin: 15px;" }

Ce blog me sert rarement de tribune, mais comme il se trouve que [korben](http://www.korben.info/) m'offre un (gros) peu de traffic
qualifié comme disent les markéteux et que le sujet me tient à coeur, j'aimerais mettre noir sur blanc ce qui me chagrine le plus dans cette
loi [HADOPI](http://hadopi.fr/) et porter à votre connaissance les effets néfastes qu'elle engendre déjà.

Tout commence par une découverte. Evidemment. Pas une fortuite qui arrive sur une faute de frappe dans les moteurs de recherche. Une vraie,
à l'heure du café, avec un chineur, un connaisseur, un mineur de fond des musiques du monde et d'ailleurs.

Ce jour là, c'est jazz manouche.

Tout part d'un truc de [Paris combo](http://fr.wikipedia.org/wiki/Paris_Combo) à l'époque je crois, puis on arrive fatalement sur les classiques du genre [Django Rheinardt](http://fr.wikipedia.org/wiki/Django_Reinhardt), [Stéphane Grappelli](http://fr.wikipedia.org/wiki/St%C3%A9phane_Grappelli) ...
que du très classique, disponibles pour 3€ à 5€ dans tous les bacs promotionnels qui se réspectent et c'est très bien comme ça.

Et tout à coup on arrive au coeur du sujet, avec un disque de [Jo Privat](http://fr.wikipedia.org/wiki/Jo_Privat), accompagné par "les
manouches de Paris" : Manouche Partie. Jo Privat est un accordéonsite, vieille France, le genre qu'on associe facilement à [Yvette](http://fr.wikipedia.org/wiki/Yvette_Horner) ...

L'accordéon à paillette, la chemise tigrée et le sourire juste prix arboré bien haut!

![Manouche de Paris](images/hadopi/manouche-de-paris.jpg){: align="right" style="margin: 15px;" }

En fait pas tout à fait! Manouche Partie représente une vraie facette de ce personnage, fasciné par la culture manouche, qui aime à jouer les
standards du genre, (les yeux noirs, nuages, ...) avec les gars du campement d'à côté.

Sur cet album d'ailleurs, s'il y a au moins un gratteux connu (Matelo Ferret), le nom d'un des violonistes est inconnu.

Enregistré en une journée, sans répétition préalable, cette galette est un monument, de l'ordre du biblique, pour tous les amoureux du jazz manouche ...

et croyez moi[il en reste](http://www.festivaldjangoreinhardt.com/)

Ca sent bon l'identité, la vraie, celle qui sait d'où elle vient, pour mieux comprendre celles des autres, celle de la France, celle du peuple
rom, ça sent le partage, le sourire, le tabac, l'alcool du repas de midi (le livret est à lire c'est Jo Privat qui raconte ...) ...

Bref ça sent la vie et l'humain.

J'espère vous avoir donné envie d'écouter ce joyaux du Jazz manouche ... car c'est le pivot de ma démonstration.

En effet vous ne pourrez pas trouver ce disque [sur amazon](http://www.amazon.com/exec/obidos/ASIN/B00005OASG/musicbrainz0d-20?v=glance&s=music),
ni sur aucun autre site de vente en ligne, ni bien entendu à la fnac \#lol. Cet album est chez quelques spécialistes et plus dans les bacs. 
[Vous apprendrez ici que la dernière édition date d'il y a 20 ans](http://musicbrainz.org/release/2b3321d9-a5a1-4b10-ac54-0b2ac6c2a7df.html).

Vous pouvez fouiller de fond en compte l'"offre légale", vous n'écouterez cet album ni sur [deezer](http://www.deezer.com/fr/), ni sur
[spotify](http://www.spotify.com/fr/new-user/) ...

Si vous trouvez un lien qui contredit mes dires, les commentaires sont ouverts: faites moi mentir!

Ma question est la suivante: pourquoi ne peut on pas avoir accès à ce disque? Pourquoi les maisons de disques, grandes gagnantes et grandes manipulatrices de l'[HADOPI](http://hadopi.fr/), n'ont aucune obligation quant à la diversité et la pérennité de leur catalogue?

Avant on trouvait cet album sur la mule, et maintenant?

Si quelqu'un du ministère de la culture me lit, les commentaires sont aussi ouverts pour lui et la réponse m'intéresse.

Si cet exemple vous fait sourire, sachez qu'il est loin d'être isolé, et que la musique n'est qu'un début dans l'appauvrissement orchestré que
 nous allons vivre.

Il y a un bouquin que j'ai adoré, parce qu'il parle de liberté, il apprend à croire en ses rêves, il rend juste la vie plus belle quand on l'a lu, et quand on l'a fini on a qu'une envie c'est l'offrir à tout son entourage. Il s'appelle le "Messie récalcitrant" ... [essayez de
l'acheter neuf pour
voir?](http://www.amazon.fr/messie-r%C3%A9calcitrant-Illusions-Richard-Bach/dp/2290339865)

<div style="text-align: center">
<img src="images/hadopi/messie.jpg" alt="Manouche de Paris" title="Manouche de Paris"/>
</div>

# EDIT

Soyons honnête! depuis ce post:

* [Manouche Partie est sur deezer](http://www.deezer.com/fr/album/7545017)
* [On trouve de nouveau le messie récalcitrant à la FNAC](https://livre.fnac.com/a111527/Richard-Bach-Le-Messie-recalcitrant)
* j'ai supprimé le filet de commentaires
* mais la lutte contre le piratage reste un trou noire culturelle pour tout ce qui n'est pas bankable!!!

Title: En finir avec Google contact
Date: 2014-09-23 16:08
Author: mazenovi
Category: vie privée
Tags: open-source, linux
Slug: en-finir-avec-google-contact
Status: published


Si vous avez décidé d’en finir avec gmail.com, vous aurez également envie d’en finir avec Google Contact. Ce service, masqué par Gmail est loin d’être anodin dans la mesure où il stocke et collecte les contacts de tous les gens avec qui vous communiquez via les services Google ... C’est ce même service qui vous permet de retrouver vos contacts « Gmail » dans votre appareil Android. Voyons comment reprendre la main là dessus.

Thunderbird est mon ami pour le mail, aussi je vous propose de l’utiliser pour la synchro initial des contacts côté client mail. Nous verrons ensuite la synchro côté Android.

## Récupérer ses données à partir de son téléphone

* Aller dans l’appli Android « Contacts »
* bouton menu « Impoter/Exporter »
* Exporter vers la carte SD
* exporter ver /storage/extSdCard/00001.vcf
* recupérer ce fichier comme vous préférez ( en connectant l’appareil en USB, via airdroid, en téléversant le fichier dans owncloud …)

## Récupérer ses données à partir de l’interface Gmail

* [https://support.google.com/mail/answer/24911?hl=fr](https://support.google.com/mail/answer/24911?hl=fr)

## Importer ses contacts dans owncloud

Maintenant vous devez avoir un fichier vcf contenant tout vos contacts. Ce fichier est importable directement dans l’app Contact d’owncloud via l’engrenage en bas à gauche

![Contacts - ownCloud](/images/ispconfig/Contacts-ownCloud.png)

## Synchroniser Thunderbird

La [doc owncloud concernant thunderbird](http://doc.owncloud.org/server/7.0/user_manual/pim/sync_thunderbird.html) est très bien faite. Elle vous expliquera comment synchroniser une partie de votre carnet d’adresses avec l’[extension Thunderbird sogo](http://www.sogo.nu/english/downloads/frontends.html) (notez que l’extension [lightning](https://addons.mozilla.org/fr/thunderbird/addon/lightning/) doit être également installée)

A noter que les contacts peuvent aussi s’importer (« Carnet d’adresse » > »Outils » > « Importer » > « Carnets d’adresses » >  « Fichier vCard (.vcf) ») à partir de Thunderbird. La présence de vos contacts dans Thunderbird peut être l’occasion de faire un peu de ménage avec par exemple l’[extension Duplicate Contact Manager](https://addons.mozilla.org/en-US/thunderbird/addon/duplicate-contact-manager/).

## Synchroniser avec Android
[CardDav Sync](https://play.google.com/store/apps/details?id=org.dmfs.carddav.sync) est l’app qui va bien pour synchroniser le carnet d’adresses de votre Android. Il suffit de sélectionner « cardDav » et d’entrée l’url du carnet d’adresse fournie via l’icone du maillon qui doit être quelque chose comme

```bash
https://your-server/path/to/owncloud/remote.php/carddav/addressbooks/youruser/contacts
```

et vos identifiants owncloud.

Un bon moyen de remettre les compteurs à zéro au niveau de votre téléphone est de supprimer l’intégralité du carnet d’adresse (après l’avoir sauver en vcf ne soyez pas kamikaze), puis d’aller dans les comptes et de désactiver toutes les autres synchro liées aux contacts (genre FB, twitter ou G+) qui sont en général plus polluantes qu’autre chose.?

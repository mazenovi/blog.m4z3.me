Title: La télé sous linux
Date: 2014-06-02 18:42
Author: mazenovi
Category: bricolage
Tags: open-source, linux
Slug: la-tele-sous-linux
Status: published

# EDIT: 2017-11-19

La plupart des outils ci après ne fonctionnent plus pour diverses raisons ...

Pour enregistrer [des podcasts et de la vidéo streamée en général](https://rg3.github.io/youtube-dl/supportedsites.html), [youtube-dl](https://rg3.github.io/youtube-dl/) semble être de loin l'outil le plus ergonomique et le plus dynamique.

A noter qu'il est disponible pour tous les OS

# Ceci étant dit ...

![TV](images/tv.jpg){: align="left" style="margin: 15px;" }

Voici
quatre applis dont je me sers régulièrement pour  regarder la télé en
live ou podacster des émissions.

## En Live

### [FreeTux TV](http://doc.ubuntu-fr.org/freetuxtv)

Cette appli vous permettra de regarder / ou
d'enregistrer la télé en live directement à partir de votre PC.

Vous ne couperez pas aux accords commerciaux: pour les freenautes par exemple vous ne pourrez regarder ni TF1, ni M6 (finalement n'est ce pas un soulagement?).

```bash
sudo add-apt-repository ppa:freetuxtv/freetuxtv  
sudo apt-get update  
sudo apt-get install freetuxtv  
```

## Les podcasts

Personnellement seuls trois m'intéressent, France Télévision, Arte et le
clair de canal+ (mais ça c'était avant)

### [Pluzzdl](http://code.google.com/p/tvdownloader/wiki/pluzzdl)

[Pluzz](http://pluzz.francetv.fr/) Regroupe les replay de france 1
(outre-mer) / france 2 / france 3 / france 4 / france5 et france Ô.  
Il se trouve que la version 0.9.4 du repo ppa:chaoswizard/tvdownloader
ne fonctionne pas et donc que la procédure d'installation indiquée sur
le site officielle installe un pluzzdl qui ne fonctione pas.

Le plus simple à l'heure actuelle est de passer par ce fork
<s><https://github.com/mattetti/pluzzdl></s>
<https://github.com/ziirish/pluzzdl>

```bash
git clone https://github.com/ziirish/pluzzdl  
cd pluzzdl  
./pluzzdl.sh -bvt
http://pluzz.francetv.fr/videos/c\_dans\_lair\_,94339884.html  
```

N.B. il vous faudra installer ffmpeg afin que pluzzdl puisse créer le
mkv final

```bash
sudo apt-get install ffmpeg  
```

si vous vous retrouvez avec une erreur lors de la création du mkv vous
pouvez toujours le créer à la main en tapant

```bash
ffmpeg -i xxx.ts -vcodec copy -acodec copy xxx.mkv  
```

[Qarte](http://forum.ubuntu-fr.org/viewtopic.php?id=861411&p=1)
---------------------------------------------------------------

Probablement le mieux fait de tous au vu de son interface conviviale.

```bash
sudo add-apt-repository ppa:vincent-vandevyvre/vvv  
sudo apt-get update  
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 89F92A1A  
sudo apt-get install qarte  
```

**Edit :** qarte semble délaissé, et les .deb sont devenus difficiles à
trouver. Vous pouvez vous rabattre sur
<https://github.com/solsticedhiver/arteVIDEOS> qui fonctionne très bien.

[Canal+.fr videos backup](https://bitbucket.org/fenollp/canal) {#markdown-header-canalfr-videos-backup}
--------------------------------------------------------------

Petit dernier trouvé, tout récemment, ce script qui permet de
télécharger les émissions en clair de canal+, telles le zapping, les
guignols etc ...

J'apprécie d'autant plus ce script, que l'appli canal sur Android me
bloque systématiquement la fin du zapping (suis je le seul?) ce qui est
particulièrement frustrant. Une fois téléchargé au moins on est sûr de
pouvoir le regarder jsuqu'au bout

```bash
git clone https://bitbucket.org/fenollp/canal.git  
cd canal  
./canal+.sh \~/Downloads
http://www.canalplus.fr/c-infos-documentaires/pid1830-c-zapping.html?vid=995699  
```

pour télécharger le zapping du 1er janvier 2014 dans le répertoire
Downloads ou encore  

```bash
./canal+.sh \~/Downloads 995699  
```

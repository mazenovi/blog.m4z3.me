Title: En finir avec Google calendar
Date: 2014-09-23 16:08
Author: mazenovi
Category: vie privée
Tags: open-source, linux
Slug: en-finir-avec-google-calendar
Status: published

Google calendar c’est pratique, c’est synchrone avec votre téléphone, votre tablette, éventuellement Votre client mail … mais finalement Google a-t-il besoin de connaître votre programme de la semaine? Voici comment vous servir d’owncloud pour garder vos calendriers synchrones et arrêter d’utiliser Google Calendar

## Récupérer ses données à partir de son téléphone

Contrairement aux contacts il n’y a pas de possibilité d’exporter ses calendriers en natifs sur Android. Heureusement [iCal import / Export](https://play.google.com/store/apps/details?id=tk.drlue.icalimportexport) fera ça très bien pour vous.
* bouton « Export »
* sélectionner « Internal/External memory » sachant que vous pouvez aussi vous envoyer le fichier par mail, ftp, ou webDav.
* sélectionner le nom et l’emplacement du fichier d’export, puis cliquez sur « Start export ».
* récupérer ce fichier comme vous préférez ( en connectant l’appareil en USB, via airdroid, en téléversant le fichier dans owncloud …)


## Récupérer ses données à partir de l’interface Google Calendar

* [https://support.google.com/calendar/answer/37111?hl=fr](https://support.google.com/calendar/answer/37111?hl=fr)

## Importer son calendrier dans ownCloud

pour importer un fichier ics dans un calendrier ownCloud il suffit de cliquer dessus  depuis l’app « Fichiers » ... il suffit de le savoir ...

![ics import](/images/ispconfig/owncloud-ics2.png)


## Synchroniser Thunderbird avec Lightning

installer [lightning](https://addons.mozilla.org/fr/thunderbird/addon/lightning/)

« Fichier » > « Nouveau » > « Agenda »

« Sur le réseau », puis sélectionner le format CalDav

copier / coller dans « Emplacement » le lien calDav obtenu en cliquant sur l’icone du maillon associé au calendrier owncloud que vous voulez gérer avec thunderbird

<div style="text-align: center;margin-bottom: 15px">
  <img src="/images/ispconfig/calendarCalDav.png">
</div>

En bonus vous obtiendrez les tâches dans lightning pour le même prix 😀

## Synchroniser avec Android

[CalDav Sync Free Beta](https://play.google.com/store/apps/details?id=org.gege.caldavsyncadapter&hl=fr) permet d’ajouter un calendrier via le menu « Paramètres » Section « Comptes » > « Ajouter compte » de votre téléphones. Il faut créer autant de compte que vous avez de calendriers à synchroniser et ça ne tient pas compte des tâches asssociées.

Pour faire fonctionner les tâches j’ai du installer [CalDav Sync](https://play.google.com/store/apps/details?id=org.dmfs.caldav.lib&hl=fr) qui lui est payant (2.59€).

Pour gérer les tâches l’app [Tasks](https://play.google.com/store/apps/details?id=org.dmfs.tasks&hl=fr) est assez élégante et fonctionne bien …

Pour finir vérifier bien que la synchronisation Android soit bien activée sur votre device, ca évite de se gratter la tête en se demandant pourquoi ça synchronise pas

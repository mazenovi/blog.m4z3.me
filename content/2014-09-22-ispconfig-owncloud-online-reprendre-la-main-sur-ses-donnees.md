Title: ispconfig owncloud chez online.net: reprendre la main sur ses données
Date: 2014-09-22 16:08
Author: mazenovi
Category: vie privée
Tags: open-source, linux
Slug: ispconfig-owncloud-chez-online-reprendre-la-main-sur-ses-donnees
Status: published


Voilà 10 ans que je fais de l’hébergement web.

Tout d’abord j’avais opté pour un hébergement de salon avec un abonnement pro chez Orange (c’était il y a longtemps), mais les performances n’étaient pas au rendez-vous. J’étais passé alors chez dédibox (désormais [online.net](http://online.net/)) et je viens de refaire une installation propre sur un [serveur entrèe de gamme](http://www.online.net/en/dedicated-server/dedibox-scg2) à base de Debian.

Mon ambition était cette fois non seulement de mettre en place une plateforme d’hébergement web et mail, mais aussi et surtout d’y ajouter quelques services maîtrisés qui me permettraient de retrouver un peu de souveraineté sur mes données: mes fichiers perso et pro, mon carnet d’adresse, mon agenda, mes communications etc …

En vérité le projet [https://arkos.io/](https://arkos.io/) est le projet qui me fait rêver en terme de données personnelles à l’heure actuelle. Mais il a deux soucis majeurs: il n’est pas fini et il implique un hébergement de salon. Ce dernier point est une bonne chose dans l’absolu, mais il me faudra  attendre la fibre pour avoir un débit montant suffisant ... dans mon cas j’en ai encore pour quelques années avant d’y être rattaché.

Je vous propose une série d’articles sensés vous permettre d’en finir avec les services tiers et de reprendre la main sur vos données pour le prix d’un serveur dédié, à savoir 9.99€ dans mon cas.

Aujourd’hui mise en place d’ISPConfig, owncloud, et backup automatiser du tout

## ISPConfig

Tout d’abord pour l’hébergement j’ai toujours aimé les solutions control panel. Après avoir utilisé [ispCPOmega](http://isp-control.net/) qui semble maintenant abandonné, j’ai switché sur [ISPConfig](http://www.ispconfig.org/page/home.html) que je trouve vraiment bien fait.

Entre autres fonctionnalités il permet via son interface de:

* gérer des virtual hosts (y compris SSL) apache
* gérer ses propres DNS
* créer des utilisateurs ssh (ou ftp)
* gérer les bases de données MySQL et les utilisateurs associés
* gérer des sauvegardes
* faire bien [d’autres trucs plus subtiles](http://www.ispconfig.org/page/en/ispconfig/services-and-functions.html) comme héberger plusieurs versions de PHP …
La ph
ilosophie c’est qu’il y a trois niveaux d’utilisation (admin, reseller, et utilisateurs) qui sont chacun limités aux opérations qui les concernent.

Ce que j’aime avec ISPConfig c’est qu’il génère une config propre plutôt que d’essayer de créer une configuration bien à lui. En d’autres termes, un admin pourra aisément reprendre la configuration générée par ISPConfig, sans ISPconfig. Il bénéficie également d’une bonne doc et d’une grosse communauté ce qui aide pas mal en cas de pépin.

Le projet est par ailleurs très actif et les mises à jour sont fréquentes et simples à appliquer.

Le point d’entrée pour se lancer est sans aucun doute les tutos [Perfect Server de how to forge](http://www.howtoforge.com/sitemap/control-panels/ispconfig) qui permettent de mettre en place l’hébergement web (php / MySQL), DNS, et mail. Voici celui qui m’a servi :

[http://www.howtoforge.com/perfect-server-debian-wheezy-apache2-bind-dovecot-ispconfig-3](http://www.howtoforge.com/perfect-server-debian-wheezy-apache2-bind-dovecot-ispconfig-3)

J’ai complété également par  cet autre tuto du même tonneau afin d’avoir roundcube comme webmail.

[http://www.howtoforge.com/using-roundcube-webmail-with-ispconfig-3-on-debian-wheezy-apache2](http://www.howtoforge.com/using-roundcube-webmail-with-ispconfig-3-on-debian-wheezy-apache2)

Ces tutos sont très bien faits, ils fonctionnent, tiennent compte des problématiques de sécurité et ne demandent qu’à être customisés par ceux qui souhaitent aller plus loin.

## owncloud

Une fois fait, j’ai regardé du côté d’ [owncloud](https://owncloud.org/) qui est un projet très intéressant quand on cherche un peu d’autonomie numérique. Je l’ai [déployé dans un espace web](https://owncloud.org/install/) créé via ISPConfig.

Notez que si c’est owncloud qui vous intéresse plus que la plateforme d’hébegement web ou mail vous pouvez sauter toute la partir Perfect server avec ISPConfig et simplement suivre ce [tuto](http://www.howtoforge.com/owncloud-7-installation-and-configuration-on-debian-7-wheezy). L’avantage est que les mises à jour owncloud se feront avec le reste des mises à jour de votre système lors d’un apt-upgrade.

Ceci dit [mettre à jour owncloud  se fait en quelques clics via l’interface](http://doc.owncloud.org/server/7.0/admin_manual/maintenance/update.html) …

## Clients Android / Desktop

Pas grand chose à signaler de ce côté là les clients existent pour toutes les plateformes Desktop et mobile. A noter dans les fonctionnalités de l’app mobile:

* une entrée dans le menu « partager » de votre device mobile qui vous permet de téléverser n’importe quel fichier de votre téléphone dans votre owncloud
* une entrée « Maintenir le fichier à jour » dans le « Détails » des fichiers accéder via l’app ownCloud (j’adore cette option qui maintient mon kdbx (fichier KeePass) à jour sur mon téléphone 😉
* une option « Activer le téléversement instantanée » qui permet d’uploader automatiquement toute photo prise avec votre appareil (à la Dropbox)

## Backup

Ce qui met la pression quand on héberge ses propres données et services c’est la sauvegarde.

Pour les sites web (contenu du répertoire + MySQL) l’interface web d’ISPconfig gère ça très bien et autorise une fréquence de sauvegarde pour chaque site et la restauration d’une sauvegarde en un clic.

Pour les mails c’est le dossier /var/vmail qui doit être sauvegardé.

J’ai également pour habitude de sauvegarder la liste des paquets installés via

~~~language-bash
dpkg --get-selections &gt; installed_packages.txt
~~~

J’ai découvert qu’online proposait un espace de sauvegarde FTP de 100Go, c’est pas le Pérou mais c’est déjà ça. D’autant qu’[une documentation sur la mise en place de la sauvegarde automatique de votre serveur online est disponible](http://documentation.online.net/fr/serveur-dedie/sauvegarde/sauvegarde-dedibackup) (et bien faite).

Personnellement j’apprécie d’avoir mes données de backup en local « au cas où », le problème est qu’une fois que vos fichiers sont sur ce serveur FTP c’est moyennement sympa pour les récupérer automatiquement en local. Sous ubuntu ce script fonctionne très bien pour récupérer toutes les archives générées (dont le nom est préfixé par isp) par backup-manager.

~~~language-bash
#!/bin/sh
HOST='dedibackup.online.net'
USER='sd-0000'
PASSWD='password'
FILE='isp*'
ftp -n -v $HOST &lt;&lt;-ENDTAG
user $USER $PASSWD
passive
prompt n
lcd /path/to/server-archives
mget $FILE
ENDTAG
~~~

## En Conclusion

Vous allez me dire que mes données ne sont plus chez Dropbox ou chez Google mais qu’elles sont tout de même bien chez Online et que je ne peux que croire qu’aucun admin n’ira y mettre son nez. C’est vrai ... mais même si les [CGV de online ne parlent pas explicitement de confidentialité](http://www.online.net/cgv.pdf) elles restent moins effrayantes que celle de [Dropbox](http://www.bodyspacesociety.eu/2014/02/22/dropbox-conditions-utilisation-exploitation-digital-labor/) ou de [Google](http://www.google.fr/intl/fr/policies/terms/regional.html) . A ce propos je vous recommande [une extension qu’on devrait toujours utiliser avant de s’inscrire à  un nouveau service](https://tosdr.org/) …

Et puis la fibre va bien finir par arriver hein?!

Title: Lubuntu recup, freebox et serveur XBMC
Date: 2013-05-06 12:59
Author: mazenovi
Category: bricolage
Tags: open-source, download
Slug: lubuntu-recup-freebox-et-serveur-xbmc
Status: published

![xbmc](images/xbmc.png){: align="left" style="margin: 15px;" }

Tout est parti d'une
conversation avec mon beauf qui voulait brancher la freeboxtv sur son
ampli pour écouter de la musique. Nous voilà parti à googler et à
trouver que quand même [18,50€ pour un
câble](http://www.fcosinus.com/Boutique/index.php?page=cdvd), c'est un
peu chérot.

Là me revient un vieille idée ... le serveur multimédia que j'ai
toujours voulu me monter.

Maintenant que j'ai acheté un disque de 2To et que j'ai compris que pour
partager un disque connecté en USB à la Freebox, il fallait le
[connecter à l'USB de la Freebox et non à l'USB du boitier
TV](http://freebox.toosurtoo.com/forum/viewtopic.php?f=68&t=2658#p51118)
j'ai presque tous les ingrédients.[  
](http://freebox.toosurtoo.com/forum/viewtopic.php?f=68&t=2658#p51118)

Concernant le choix du media center, c'est mon platrier (si! si! il s'y
connaît vachement, c'est lui qui m'a montré comment flashé ma ROM de
Galaxy S)  qui m'avait conseillé [xbmc](http://xbmc.org/).

Un instant je me suis dit qu'un [raspberryPi comme serveur
xbmc](http://wiki.xbmc.org/index.php?title=Raspberry_Pi) ca devait être
fun ...

Souvent dans ces cas là, j'essaie de réfrainer l'achat et de penser
d'abord récup. J'ai un [SONY
VPCX11Z1E](http://www.pcadvisor.co.uk/reviews/laptop/3224636/sony-vaio-x-vpcx11z1e-x-review/)
qui ne me sert à rien car c'est une vraie charette, ce sera mon dernier
essai d'en faire quelque chose.  
Dans un premier temps je cherche donc un système qui réponde de manière
acceptable sur cette machine (c'est à dire pas ubuntu) et qui gère le
chipset graphique GMA500. Sur ce dernier point j'ai plutot du bol dans
la mesure où le [chipset GMA500 est géré en natif depuis Natty (ubuntu
12.10)](https://wiki.ubuntu.com/HardwareSupportComponentsVideoCardsPoulsbo).

Après avoir testé une [debian en mode texte avec un serveur X pour
afficher la fenêtre
xbmc](http://myhpmicroserver.com/wiki/Install_XBMC_on_Debian_Squeeze#On_Headless_Debian)
(qui n'a pas de version "headless" dommage),
[xubuntu](http://xubuntu.org/), mon choix s'arrête sur
[lubuntu](http://lubuntu.net/). Je recommande vraiment cette distro pour
donner une seconde vie à toute bécane obsolète qui traîne dans un
placard. Reste à installer xbmc.

```bash
sudo apt-get install xbmc
```

Xbmc est extrêment gourmand en terme de CPU ce qui se traduit par un
lagge insupportable du curseur dans la fenêtre xbmc. [Ce tweak de
configuraiton xbmc améliore pas mal les choses \[valable uniquement pour
Xbmc 11
(Eden)\]](http://thepcspy.com/read/how-fix-idle-100-cpu-issue-xbmc/)

J'ai personnellement constaté que xbmc 11 (Eden) est moins gourmand que
xbmc 12 Frodo. Il se trouve que c'est [xbmc 11 qui est par défaut dans
les repo
ubuntu](http://wiki.xbmc.org/index.php?title=XBMC_v12_%28Frodo%29_FAQ#Why_is_Frodo_not_in_the_default_Debian.2FUbuntu_repositories.3F).
Je n'ai donc pas chercher à [upgrader vers xbmc
12](http://wiki.xbmc.org/index.php?title=XBMC_for_Linux_specific_FAQ#Ubuntu_manual_XBMC_upgrade_process)
et j'ai même créer un fichier /etc/apt/preferences qui interdira les
mises à jour futures

```bash
Package: xbmc  
Pin: version 11.0  
Pin-Priority: 1000  
```

Ce choix rétrograde aura une conséquence par la suite ...

Le dernier allègement possible est de virer les pseudo equalizer qui
s'affiche quand un morceau se joue et de ne rien faire afficher à la
lecture des morceaux.

<div style="text-align: center; margin-bottom: 20px">
    <img src="images/xbmc/noimage1.png" />
</div>

<div style="text-align: center; margin-bottom: 20px">
    <img src="images/xbmc/noimage2.png" />
</div>

Un petit peu de conf système histoire de rendre les choses un peu plus
agréables

* [Autologin](doc.ubuntu-fr.org/lubuntu#a_partir_de_lubuntu_1204_lightdm)
* Montage automatique des partages de la freebox:  
   installer de quoi monter les partitions windows  

```bash
sudo apt-get install cifs-utils  
```
   paramétrage du montage automatique des disques connectés à la
    freebox  

```bash
UUID=9eaa53a4-0f2c-462d-87e8-1eedd89c856a / ext4 errors=remount-ro 0 1  
UUID=dabca218-5eed-42fa-a882-e119428b5aeb none swap sw 0 0  
//192.168.0.250/DataMaze2To /home/mazenovi/DataMaze2To cifs rw,users,_netdev,iocharset=utf8,uid=1000,file_mode=0777,dir_mode=0777 0 0  
//192.168.0.250/disque\\040dur /home/mazenovi/freebox cifs rw,users,_netdev,iocharset=utf8,uid=1000,file_mode=0777,dir_mode=0777 0 0  
```

   où freebox est le disque dur de la freeboxtv et Datamaze2To est le
    disque de 2To qui est connecté à la freebox en USB.  
   on remonte tout pour voir si ça marche  

```bash
sudo mount -a  
```

* démarrage automatique d'XBMC - comme j'ai ajouté XBMC au bureau:  

```bash
cp /home/mazenovi/Bureau/xbmc.desktop \~/.config/autostart/  
```

* en bonus le [remote desktop](https://wiki.edubuntu.org/Lubuntu/RemoteDesktop)

Pour ajouter les dossiers contenant votre musique (ou vos vidéos) Home
&gt; Music &gt; Files &gt; Add source

Pour écouter de la musique ce que je veux c'est un télécommande sur mon
téléphone Android. [J'installe donc l'appli XBMC
officielle](https://play.google.com/store/apps/details?id=org.xbmc.android.remote).
Le problème c'est que cette version est conçue pour xmbc 12 Frodo et
qu'il y a eu pas mal de changement dans l'Api entre les deux version.
C'est pourquoi je suis allé chercher une [version
antérieure](http://runamux.net/down/view/android/VA3Zr0_u/orgxbmcandroidremote-808-088-b.html).

System &gt; Settings &gt; Network et configurer quelque chose comme la
copie d'écran ci dessous

<div style="text-align: center; margin-bottom: 20px">
    <img src="images/xbmc/settings.png" />
</div>

Une fois le problème de la télécommande réglée il est aussi bien
agréable de pouvoir piloter sont serveur à partir d'une machine. Je vous
laisse découvrir et installer
[maraschino](http://www.maraschinoproject.com/) qui fait ça très bien
...

<div style="text-align: center; margin-bottom: 20px">
    <img src="images/xbmc/maraschino-1024x767.png" />
</div>

Edit
====

En passant à lubuntu 13 (raring), j'ai finalement écopé de Frodo (XBMC
12). C'est moins catastrophique que je ne le pensais en terme de
performance. Après mise à jour de xbmc remote sur Android je n'ai juste
plus les pochettes d'albums sur mon téléphone.

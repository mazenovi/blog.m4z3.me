Title: PGP Batman gère ses clés
Date: 2018-01-16 08:34
Author: mazenovi
Category: vie-privee
Tags: crypto, gpg
Status: draft

[TOC]

Batman vous livre ici tous ces secrets pour utiliser gpg simplement avec efficacité

## Maîtriser son environnement gpg

Pour faciliter la prise en main Batman de gpg utilise **gpgenv**, un petit outil développé par [@themouette](https://github.com/themouette) qui lui permet:

* de créer a tout moment un environnement vierge pour gpg
* de sauvegarder un environnement gpg modifié (ajout de clés publics, révocation, etc ...)
* de restaurer un environnement sauvegardé

Ca n'a l'air de rien mais ça lui simplifie grandement la vie: il peut jouer facilement avec gpg pour mieux le comprendre, sans avoir peur de perdre ses clés privés et sa toile de confiance et il peut importer / exporter son environnement PGP en une commande pour le modifier (sur un système live comme [tails](https://tails.boum.org/index.fr.html) par exemple ce qui est recommandé)

Pour obtenir gpgenv

```
$ wget https://raw.githubusercontent.com/themouette/dotfiles/master/zsh/zshrc.d/gnupg -O /tmp/gpgenv
$ source  /tmp/gpgenv
```

ensuite ça s'utilise comme ça

```
$ gpgenv help

Manage gnupg environments.

 Usage: gpgenv [command]


 Exemples:
       gpgenv help
       gpgenv use /tmp/gnupg
       gpgenv use
       gpgenv cd
       gpgenv create
       gpgenv create /tmp/gnupg
       gpgenv show
       gpgenv backup /media/secret-store/gnupg-$(date  +%Y-%m-%d).tgz
       gpgenv restore /media/secret-store/gnupg-2018-01-09.tgz

```

```
$ gpgenv create
new GNUPGHOME=/tmp/gnupg-Lw4Ck
```

crée un environnement vierge localisé dans /tmp/gnupg-Lw4Ck.

```
$ gpgenv cd
```

Et hop! un nouvel environnement gpg temporaire ;).
Si vous faites un ls, vous vous apercevrez qu'il n'y a aucun fichier, c'est un environnement vierge.

```
$ gpg --help

```

permet notamment de connaître la version de gpg utilisée, ici ```gpg 2.1.11```


## Création d'un nouveau trousseau

```
$ gpg --full-gen-key
gpg (GnuPG) 2.1.11; Copyright (C) 2016 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Sélectionnez le type de clés désiré :
   (1) RSA et RSA (par défaut)
   (2) DSA et Elgamal
   (3) DSA (signature seule)
   (4) RSA (signature seule)
Quel est votre choix ? 1
```

Batman cherche tout d'abord générer une clés maîtresse (*master key*)


```
les clés RSA peuvent faire une taille comprise entre 1024 et 4096 bits.
Quelle taille de clés désirez-vous ? (2048) 4096
```

Ici prendre le maximum revient à repousser le moment où la puissance de calcul des machines sera suffisante pour casser cette clés: plus on choisit long, plus on repousse l'échéance.

```
La taille demandée est 4096 bits
Veuillez indiquer le temps pendant lequel cette clés devrait être valable.
         0 = la clés n'expire pas
      <n>  = la clés expire dans n jours
      <n>w = la clés expire dans n semaines
      <n>m = la clés expire dans n mois
      <n>y = la clés expire dans n ans
Pendant combien de temps la clés est-elle valable ? (0)
La clés n'expire pas du tout
Est-ce correct ? (o/N) o
```

Batman est en train de générer sa clés maîtresse, celle à laquelle il ne doit rien arriver et qui n'est sur aucune de ces machines ... autant qu'elle dure longtemps: il serait dommage qu'elle soit expirée le jour où il veut s'en servir pour révoquer une clés compromise.

De plus, plus la durée d'une clés maîtresse (qui sert aussi de certificat et qui donc récolte les signatures des autres clés qui font confiance à Batman) est longue, plus elle récoltera de signature et plus sa confiance sera consolidée.

Pour ces deux raisons Batman choisi une clés qui n'expire jamais.

```
GnuPG doit construire une identité pour identifier la clés.

Nom réel : Batman
Adresse électronique : batman@batcave.com
Commentaire : clés à usage professionnel
Vous utilisez le jeu de caractères « utf-8 ».
Vous avez sélectionné cette identité :
    « Batman (clés à usage professionnel) <batman@batcave.com> »

Changer le (N)om, le (C)ommentaire, l'(A)dresse électronique
ou (O)ui/(Q)uitter ? O
De nombreux octets aléatoires doivent être générés. Vous devriez faire
autre chose (taper au clavier, déplacer la souris, utiliser les disques)
pendant la génération de nombres premiers ; cela donne au générateur de
nombres aléatoires une meilleure chance d'obtenir suffisamment d'entropie.
```

Ici il faut choisir un mot de passe fort, dont vous vous souviendrez ... et attendre un petit peu que la machine génère la clés

```
gpg: clés E0A18868 marquée de confiance ultime.
gpg: revocation certificate stored as '/tmp/gnupg-Lw4Ck/openpgp-revocs.d/FE3FD10D4C60384937FDE7C6C5FA9F33E0A18868.rev'
les clés publique et secrète ont été créées et signées.

gpg: vérification de la base de confiance
gpg: marginals needed: 3  completes needed: 1  trust model: PGP
gpg: profondeur : 0  valables :   2  signées :   0
     confiance : 0 i., 0 n.d., 0 j., 0 m., 0 t., 2 u.
pub   rsa4096/E0A18868 2018-01-09 [S]
      Empreinte de la clés = FE3F D10D 4C60 3849 37FD  E7C6 C5FA 9F33 E0A1 8868
uid        [  ultime ] Batman (clés à usage professionnel) <batman@batcave.com>
sub   rsa4096/3EF7A2F9 2018-01-09 []
```

C'est fait Batman a sa clés maîtresse.

Cette fois s'il liste les clés dans son environnement

```
$ gpg --list-keys
/tmp/gnupg-Lw4Ck/pubring.kbx
----------------------------
pub   rsa4096/3E5AC6A0 2018-01-09 [SC]
uid        [  ultime ] Batman (clés à usage professionnel) <batman@batcave.com>
sub   rsa4096/31C5E7B9 2018-01-09 [E]

```

et s'il liste uniquement les clés privées

```
$ gpg --list-private-keys
/tmp/gnupg-Lw4Ck/pubring.kbx
----------------------------
pub   rsa4096/3E5AC6A0 2018-01-09 [SC]
uid        [  ultime ] Batman (clés à usage professionnel) <batman@batcave.com>
sub   rsa4096/31C5E7B9 2018-01-09 [E]

```

Euh ça voudrait dire que Batman n'a pas de clés publique?
Comment va t on pouvoir le contacter en cas d'urgence à Gotham City?

```
$ gpg --export --armor batman@batcave.com > batman@batcave.com.public.key
```

Hop la partie publique de sa clés maîtresse est dans le fichier ```batman@batcave.com.public.key```.

### Ajouter une photo

### Ajouter une identité

Comme on l'a vu Batman ne souhaite absolument pas dévoilé qu'il est Bruce Wayne, or Bruce Wayne, en tant que milliardaire à lui même des messages qui doivent rester confidentiels. Il lui faut donc aussi utiliser PGP avec l'identité de Bruce Wayne.

Il y a deux façon de gérer ses identités sur PGP:

1. ajouter une identité supplémentaire à une clé existante


Cette pratique permet notamment de regrouper plusieurs mails dans un seul trousseau
Le problème, dans le cas de Batman c'est que qu'il n'y a pas moyen de cloisonner les identités au sein d'une clé.
Ce n'est donc pas la bonen soluton pour Batman.

2. se générer un nouveau trousseau composée d'une nouvelle paire de clés avec un nouveau mot de passe

### Sélectionner les algorithmes cryptographiques

### Création d'une sous clé

Batman a une clés maîtresse mais il se dit que ce n'est sûrement pas cette clés qu'il doit mettre sur son galaxy S8 edge coque platine.
S'il l'oublie n'importe qui pourra se faire passer pour lui auprès de Robin, vous imaginez le bordel!

Donc il se créée une sous clés dédiée à son mobile.

```
$ gpg --edit-key batman@batcave.com
gpg (GnuPG) 2.1.11; Copyright (C) 2016 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

La clés secrète est disponible.

sec  rsa4096/3E5AC6A0
     créé : 2018-01-09  expire : jamais       utilisation : SC  
     confiance : ultime        validité : ultime
ssb  rsa4096/31C5E7B9
     créé : 2018-01-09  expire : jamais       utilisation : E   
[  ultime ] (1). Batman (clés à usage professionnel) <batman@batcave.com>

gpg> addkey
Sélectionnez le type de clés désiré :
   (3) DSA (signature seule)
   (4) RSA (signature seule)
   (5) Elgamal (chiffrement seul)
   (6) RSA (chiffrement seul)
Quel est votre choix ? 4
```
Là Batman réfléchit ...

* quel est l'intérêt d'une sous clé de chiffrement?

* quel est l'intérêt d'une sous clé de signature?

```
les clés RSA peuvent faire une taille comprise entre 1024 et 4096 bits.
Quelle taille de clés désirez-vous ? (2048) 4096
La taille demandée est 4096 bits
Veuillez indiquer le temps pendant lequel cette clés devrait être valable.
         0 = la clés n'expire pas
      <n>  = la clés expire dans n jours
      <n>w = la clés expire dans n semaines
      <n>m = la clés expire dans n mois
      <n>y = la clés expire dans n ans
Pendant combien de temps la clés est-elle valable ? (0) 0
La clés n'expire pas du tout
Est-ce correct ? (o/N) o
Faut-il vraiment la créer ? (o/N) o
De nombreux octets aléatoires doivent être générés. Vous devriez faire
autre chose (taper au clavier, déplacer la souris, utiliser les disques)
pendant la génération de nombres premiers ; cela donne au générateur de
nombres aléatoires une meilleure chance d'obtenir suffisamment d'entropie.
Il n'y a pas suffisamment d'octets aléatoires disponibles. Veuillez faire
autre chose pour que le système d'exploitation puisse rassembler plus
d'entropie (187 octets supplémentaires sont nécessaires).
```

Batman choisit ici un **autre** mot de passe fort

```
sec  rsa4096/3E5AC6A0
     créé : 2018-01-09  expire : jamais       utilisation : SC  
     confiance : ultime        validité : ultime
ssb  rsa4096/31C5E7B9
     créé : 2018-01-09  expire : jamais       utilisation : E   
ssb  rsa4096/DD6A21D4
     créé : 2018-01-09  expire : jamais       utilisation : S   
[  ultime ] (1). Batman (clés à usage professionnel) <batman@batcave.com>
```

Batman a maintenant une sous clés de signature (il n'a besoin que de ça sur son téléphone pour communiquer avec GPG)

```
gpg> save
```

Comme pour tout le monde il ne faut pas oublier de sauver. Sinon les modifs sont perdus.

Il reste maintenant à exporter la partie privée clés maîtresse afin de pouvoir éventuellement la réimporter (avec gpgenv c'est presque superflu, mais on est jamais trop prudent)

```
$ gpg --export-secret-keys --armor batman@batcave.com > batman@batcave.com.private.key
```
Notez que Batman doit saisir le mot de passe de la partie privée de sa clés maîtresse **et** le mot de passe de la sous clés qu'il vient de créer.

Autant dire que le fichier ```batman@batcave.com.private.key``` Batman le met sur deux clés USB qu'il stocke dans un coffre fort.

Rien ne doit arriver à ce fichier!

Maintenant qu'elle est sauvegardée il s'agit de *nettoyer* la clés maîtresse pour n'obtenir que la sous clés. Ce sera ce que Batman mettra sur son téléhpone.

```
$ gpg --export-secret-subkeys --armor DD6A21D4 batman@batcave.com > batman@batcave.com.mobile.private.subkey
```

Notez que Batman, ici aussi doit saisir le mot de passe de la partie privée de sa clés maîtresse **et** le mot de passe de la sous clés qu'il vient de créer.

Notez aussi que Batman a spécifié l'ID de la sous clés qu'il voulait exporter: en effet il a pour projet de se faire, sur le même principe, une clés pour son ordinateur fixe. Attendu que son ordianteur est dans la batcave il a beaucoup moins de chance d'être volé et encore moins d'être perdu que son mobile. S'il oublie son mobile au bistrot il ne sera pas obligé de révoquer son fixe pour autant.

Sans l'ID ```gpg --export-secret-subkeys``` exporte toutes les sous clés ...

Batman va donc sauver son environnement GPG courant

```
$ gpgenv backup ~/Bureau/batman.tgz
```

## Publication d'une clé publique

Le fichier ```batman@batcave.com.public.key``` est à publier sans modération afin que chacun puisse joindre Batman en cas de problème en toute sécurité. Mais où publier sa clés PGP, sur son site web? C'est une bonne idée mais cela oblige les utilisateurs à revenir de temps en temps pour vérifier si la clés n'a pas été révoqué,  ou si elle n'a pas obtenu de nouvelles signatures.

### Serveurs de clé PGP

Les serveurs de clés PGP existent depuis que PGP existe [http://pgp.mit.edu/](http://pgp.mit.edu/) est sans doute le plus connu. L'avantage c'est qu'ils sont simples d'utilisation.

Ils présentent toutefois deux inconvénients majeurs:

1. n'importe qui peut publier des clés publiques pour n'importe quelle identité puisqu'il n'y a aucune vérification
2. il n'y a aucun moyen de supprimer une clé obsolète dont par exemple on aurait perdu le mot de passe.

### Serveur de clé PGP Global Directory

Ce service répond aux deux inconvénients des serveurs de clés historiques, en mettant en place une vérification mail:
1. lors de la création un mail est envoyé au(x) mail(s) associé(s) à la clés pour confirmation. Ce qui a le mérite de valider que la clé publiée appartient bien à l'utilisateur maitrisant le(s) mail(s) associé(s) à la clés.
2. un mail est envoyé au(x) mail(s) associé(s) à la clés à intervalles régulier pour confirmer que cette clé est toujours d'actualité. Sans réponse la clé est supprimée ce qui évite l'accumulation de clés obsolète.

### Keybase.io

[Keybase.io](https://keybase.io) est assez récent et basé sur PGP.

Il permet de publier sa clé publique et de prouver qu'il appartient à l'utilisateur maitrisant le(s) compte(s) sociau(x) proposés par Keybase.io.

Il permet également de communiquer /partager simplement avec d'autres utilisateurs en chiffrant avec sa clé PGP, via un PC / Mac ou via son téléphone.

Seulement pour ça il faut laisser sa clé privée sur leur serveur et ça Batman il a bien lu partout que ça ne se faisait pas

Même si on jure qu'on le fait du mieux qu'on peut

![KeyBase - host private key](/images/pgp/keybase1.png)

Batman choisit donc ici "Maybe another time" car il ne souhaite qu'associer sa clé à son compte reddit que les utilisateurs de [/r/GothamCityChat/](https://www.reddit.com/r/GothamCityChat/) connaissent bien. N'importe quel habitant de Gotham vous le dira ce compte reddit est forcément celui du vrai batman.

Ensuite il faut bien que Batman prouve qu'il posssède la partie privée de la clés qu'il vient d'importer et comme il n'a pas choisit d'importer sa clés privée sur le serveur il doit le faire via la ligne de commande. Comme il n'a pas confiance dans la ligne de commande keybase il choisit d'utiliser la commande gpg de son système

![KeyBase - prove you have the private key](/images/pgp/keybase2.png)

![KeyBase - prove you have the private key(2)](/images/pgp/keybase3.png)

En exécutant cette commande Batman communique la signature demandée par keybase en la générant avec gpg et en l'envoyant à keybase avec curl: ça marche.

En exécutant le même type de commande Batman génère une texte que keybase.io lui demande de publier sur son reddit.

Finalement keybase.io est en mesure d'affirmer que le possesseur de la clés publique que Batman vient d'importer est bien contrôler par le même utilisateur qui contrôle le compte reddit.

Importer une sous clé permettrait il d'utiliser les services de chiffrement de communicaiton et de partage de keybase.io en réduisant les risques?

## Importation d'une clé privée

### Importer sa sous clés sur un PC ou un Mac

Batman créée un environnement PGP vierge
```
$ gpgenv create
```
puis il importer sa sous clés uniquement (qu'il a extrait de ~/Bureau/batman.tgz dasn /tmp)

```
$ gpg --import /tmp/batman@batcave.com.mobile.private.subkey
```

il vérifie alors que la partie privée de sa clés maîtresse est inutilisable

```
$ gpg --list-secret-keys
/tmp/gnupg-RL2R8/pubring.kbx
----------------------------
sec#  rsa4096/3E5AC6A0 2018-01-09 [SC]
uid        [ inconnue] Batman (clés à usage professionnel) <batman@batcave.com>
ssb   rsa4096/31C5E7B9 2018-01-09 [E]
ssb   rsa4096/DD6A21D4 2018-01-09 [S]
```

le # signifie que la partie privée de la clés maîtresse est inutilisable.

Pour s'en persuader Batman tente de modifier sa sous clé

```
$ gpg --edit-key DD6A21D4
gpg (GnuPG) 2.1.11; Copyright (C) 2016 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

La clés secrète est disponible.

pub  rsa4096/3E5AC6A0
     créé : 2018-01-09  expire : jamais       utilisation : SC  
     confiance : inconnu       validité : inconnu
ssb  rsa4096/31C5E7B9
     créé : 2018-01-09  expire : jamais       utilisation : E   
ssb  rsa4096/DD6A21D4
     créé : 2018-01-09  expire : jamais       utilisation : S   
[ inconnue] (1). Batman (clé à usage professionnel) <batman@gmail.com>

gpg> adduid
Nom réel : Bruce Wayne
Adresse électronique : bruce.wayne@gmail.com
Commentaire : clé à usage personnel
Vous utilisez le jeu de caractères « utf-8 ».
Vous avez sélectionné cette identité :
    « Bruce Wayne (clé à usage personnel) <bruce.wayne@gmail.com> »

Changer le (N)om, le (C)ommentaire, l'(A)dresse électronique
ou (O)ui/(Q)uitter ? O
gpg: échec de la signature : Pas de clés secrète
gpg: signing failed: Pas de clés secrète
```

impossible! Bonne nouvelle ;)

C'est gagné il n'y a plus qu'à importer la clés sur son mobile.

Notez qu'avec les commandes que l'on vient de voir Batman va pouvoir se créer une sous clés dédiée pour son fixe les doigts dans le nez :D

### Importer une clé PGP sur un mobile

Pour gérer sa clés sur son mobile Batman utilise [OpenKeyChain](https://www.openkeychain.org/), c'est sous licence [GPLv3](https://github.com/open-keychain/open-keychain/blob/HEAD/LICENSE), il a pu jeter un oeil au [code source](https://github.com/open-keychain/open-keychain) et en plus l'app est disponible sur le store [F-Droid](https://f-droid.org/packages/org.sufficientlysecure.keychain/) ce qui est une garantie supplémentaire.

Une fois qu'il a trouvé l'app pour gérer sa clésil ne lui reste plus qu'à l'importer sur sont téléphone.

Il se dit que se l'envoyer par mail serait simple mais dévoilerait sa clés à son service mail (et en l'occurrence il sait que gmail lit ses courriels ...).

Sinon il peut transférer de fichier en branchant son mobile via l'usb: c'est une option.

Mais comme in est un peu geek, il cherche un peu sur le web et tombé sur [asc2qr](https://github.com/4bitfocus/asc-key-to-qr-code).

Mais ça ne marche pas. En creusant un peu parce qu'il est vraiment geek il est tombé sur ce commentaires

https://security.stackexchange.com/questions/70501/putting-my-pgp-id-link-on-printed-business-cards

et il se dit qu'avec la commande [qrencode](https://doc.ubuntu-fr.org/qrcode) il doit y avoir de faire ça à l'aise?

Quelle sont les commandes à exécuter:

- Pour exporter la partie publique de la clés maîtresse (le certificat)?
- Pour exporter la partie privée de la clés maîtresse?
- Pour exporter la partie privée de la sous clés dédiée?

Et si Batman avait un iPhone?

## Révocation

Il peut arriver malheur à une clé privée ou à une sous clé: elles peuvent être perdues, on peut oublier le mot de passe.

PGP gère ce cas avec un **certificat de révocation** pour la clé. Ce certificat n'est autre que la clé publique qui présente la liste des sous clés associées en mentionnant leur date d'expiration ou éventuellement le fait qu'elles aient été révoquées.

Charge ensuite à Batman de diffuser de la manière la plus large possible le **certificat de révocation**. Il peut pour cela publier ce certificat sur les serveurs de clés mentionnées plus haut.

Il faut bien comprendre de chose:

1. le mot de passe de la clé privée est nécessaire pour générer le certificat de révocation, il est donc conseillé de le générer dès la création de la clé (mot de passe perdu = certificat de révocation impossible)

2. une fois le certificat de révocation diffuser il n'est plus possible d'utiliser le trousseau, la confiance de tout utilisateur dans ce trousseau sera nulle et de fait inutilisable. Tout message chiffré ou signature créer avant la révocation reste toutefois exploitable.

https://security.ias.edu/revoking-subkey

### perte de la clé maître

Mais quid d'Alfred le major d'homme? En tant que tuteur légal c'est le seul à posséder les clés du coffre où sont stockées les deux clés USB contenant la partie privée de la clé maîtresse

```
$ gpg --output vincent.mazenod@.isima.fr.revoke.asc --gen-revoke vincent.mazenod@isima.fr

sec  4096R/0DCD0D14 2017-09-04 Vincent Mazenod (Professional key for ISIMA/LIMOS) <vincent.mazenod@isima.fr>

Faut-il créer un certificat de révocation pour cette clés ? (o/N) o
choisissez la cause de la révocation :
  0 = Aucune raison indiquée
  1 = La clés a été compromise
  2 = La clés a été remplacée
  3 = La clés n'est plus utilisée
  Q = Annuler
(Vous devriez sûrement sélectionner 1 ici)
Quelle est votre décision ? 1
Entrez une description facultative, en terminant par une ligne vide :
> WARNING DON'T TRUST THIS KEY ANYMORE
>
Cause de révocation : La clés a été compromise
WARNING DON'T TRUST THIS KEY ANYMORE
Est-ce d'accord ? (o/N) o

Une phrase secrète est nécessaire pour déverrouiller la clés secrète de
l'utilisateur : « Vincent Mazenod (Professional key for ISIMA/LIMOS) <vincent.mazenod@isima.fr> »
clés RSA de 4096 bits, identifiant 0DCD0D14, créée le 2017-09-04

sortie forcée avec armure ASCII.
Certificat de révocation créé.

Veuillez le déplacer sur un support que vous pouvez cacher ; toute personne
accédant à ce certificat peut l'utiliser pour rendre votre clés inutilisable.
Imprimer ce certificat et le stocker ailleurs est une bonne idée, au cas où le
support devienne illisible. Attention quand même : le système d'impression
utilisé pourrait stocker ces données et les rendre accessibles à d'autres.
```

Se persuader que enigmail et la gestion des clés par gpg sont complètement déconnecté

### perte d'une sous clé

```
$ gpg --output ~/shared/gpg-isima/revoke --generate-revocation batman@gmail.com
```

## Liens

* https://alexcabal.com/creating-the-perfect-gpg-keypair/
* http://openpgp.vie-privee.org/gpg-intro-4.htm

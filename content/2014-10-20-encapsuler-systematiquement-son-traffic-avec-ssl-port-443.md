Title: Encapsuler systématiquement son traffic avec SSL
Date: 2014-10-20 10:32
Author: mazenovi
Category: sécurité
Tags: open-source, linux
Slug: encapsuler-systematiquement-son-traffic-avec-ssl-port-443
Status: published

![openvpn1](images/openvpn.png){: align="left" style="margin: 15px;" }

J'ai passé 5 minutes à voir les différentes solutions envisageables pour
encapsuler son traffic avec SSL que ce soit pour des raisons de sécurité
sur un wifi public par exemple ou pour des raisons de vie privée sur un
réseau professionnel.

L'idée est d'avoir une solution  simple à mettre en place et à utiliser:
j'avoue que les fichiers de conf avec des tweaks interminables me gavent
un peu.

L'autre contrainte est d'avoir une solution tout terrain, c'est à dire
utilisable sur un maximum de réseau, ce qui se résume à pouvoir faire
passer le traffic encapsulé par un port ouvert sur tous les réseaux ...
le 443 paraît un bon candidat

le VPN avec [OpenVPN Access Server](http://openvpn.net/index.php/access-server/overview.html)
=============================================================================================

Côté mise en place simple, j'ai découvert
[openvpn-as](http://openvpn.net/index.php/access-server/overview.html)
qui simplifie et accélère considérablement l'installation d'un serveur
VPN côté serveur et côté client.

Ce [tuto](http://www.unixmen.com/install-openvpn-asaccess-server-on-ubuntu-debian/ "http://www.unixmen.com/install-openvpn-asaccess-server-on-ubuntu-debian/")
montre les quelques étapes pour une installation rapide et une config
minimale fonctionnelle. Une fois qu'on a fait ça on a déjà moyen
d'encapsuler tout  son traffic entre la machine sur laquelle est
installé le client et le serveur VPN. On peut donc être plus serein vis
à vis des petits malins qui snifferaient votre traffic sur un wifi ...
ils se casseront les dents sur SSL (je ne couvre pas le cas ou vous êtes
recherché par le FBI et qu'un expert NSA a pris la chambre attenante à
la vôtre).

Cette solution est assez rutilante et fonctionne très bien mais elle a
un défaut c'est que le clien OpenVPN doit être installé avec les
permissions administrateurs. Sous windows notamment l'installeur va
installer des pilotes, ce qui rend la version portable d'OpenVPN
beaucoup moins intéressante ...

L'autre aspect qui ne nous arrange pas est qu'OpenVPN tourne par défaut
sur le port 1194 ce qui nous rend tributaire de ce port

Reroutage systématique du traffic via ssh
=========================================

Ce
[tuto](http://linuxaria.com/article/use-ssh-more-secure-browsing-public-networks?lang=en)
explique comment créer un serveur proxy à partir du démon sshd et
comment configurer ensuite ce proxy dans son navigateur. Cette solution
me paraissait assez légère. Mais après réflexion elle ne permet de
chiffrer que le traffic web et finalement je ne suis pas sûr que le port
22 soit plus ouvert satistiquement parlant que le port 1194. D'autre
part le fait qu'il faille lancer un démon nécessite aussi les droits
d'administrateur ... [y compris sous
windows](http://www.makeuseof.com/tag/how-to-tunnel-traffic-with-ssh/)
...

Passer par le port 443
======================

Le port 443 est traditionellement le port https. La suite s'intéresse
aux moyens de faire cohabiter ce serveur https et d'autres services
(typiquement ssh et / ou openVPN) sur ce port

Multiplexage avec sshl
----------------------

Une solution est le mutiplexage sur le port 443, c'est à dire qu'on va
chercher à faire en sorte de distribuer les paquets recus sur le port
443 aux démons ssh et openvpn.

[sslh](http://www.rutschle.net/tech/sslh.shtml) est conçu pour ça, [il
analyse la morphologie des
paquets](http://linuxfr.org/news/sslh%C2%A0110-la-b%C3%AAte-noire-des-censeurs#toc_3)
et est capable de rerouter le traffic sur les services suivants : ssh,
openVPN, Tinc et XMPP

Comme dans mon cas le port 443 est déjà occupé par les vhost apache en
https, il faut changer le port https par défaut du serveur et des Vhost

```bash
vi /etc/apache2/ports.conf  
```

remplacer

```bash
Listen 443  
```

par

```bash
Listen 4443  
```

il faut ensuite modifié les vhost ssl en conséquences:

```bash
find /etc/apache2/sites-available/ -type f -exec sed -i 's/443/4443/g'
{} \\;  
```

Comme j'utilise ipsconfig j'ai pris la peine de modifier les templates
pour ne plus avoir à m'en préoccuper

```bash
vi /usr/local/ispconfig/server/conf/vhost.conf.master  
```

remplacer

```bash
<virtualhost {tmpl_var name="ip_address"}:{tmpl_var name="port"}>
```

par

```bash
<tmpl_if name="port" op="==" value="80">  
<virtualhost {tmpl_var name="ip_address"}:80 >  
<tmpl_else>  
<tmpl_if name="port" op="==" value="443">  
<virtualhost {tmpl_var name="ip_address"}:4443>  
</tmpl_if>  
</tmpl_if>
```

J'ai adapté selon la même logique le fichier
/usr/local/ispconfig/server/conf/apache_ispconfig.conf.master

remplacer


```bash
NameVirtualHost *:443
```

par

```bash
NameVirtualHost *:4443
```

et

```bash
NameVirtualHost {tmpl_var name="ip_address"}:{tmpl_var name="port"}
```

par

```bash
<tmpl_if name="port" op="==" value="80">  
NameVirtualHost {tmpl_var name="ip_address"}:80  
<tmpl_else>  
<tmpl_if name="port" op="==" value="443">  
NameVirtualHost {tmpl_var name="ip_address"}:4443  
</tmpl_if>  
</tmpl_if>
```

il est aussi nécessaire de retoucher le vhost ```/etc/apache2/sites-enabled/000-ispconfig.conf```
en remplaçant la valeur ```443``` par ```4443```

Attention ces modifications seront à effectuer après [chaque mise àu
jour
d'ISPconfig](http://www.faqforge.com/linux/controlpanels/ispconfig3/how-to-update-ispconfig-3/)

Pour régénérer tout les vhosts il suffit de passer par ispconfig (qui
lui tourne par défaut en https sur le port 8080 donc vous devez pouvoir
l'attraper) puis d'aller dans "Tools" &gt; "Resync" et cocher "Resync
Websites" pour forcer la génération de tous les vhosts.

Bref pour ce qui est de [configurer sslh pour multiplexer ssh et https
sur le port
443](http://memo-linux.com/sslh-https-et-ssh-sur-le-meme-port/) il n'y a
eu aucun problème. en Revanche même avec des [règlages
fins](https://gist.github.com/hostmaster/7819751) je n'ai pas réussi à
mettre openVPN en écoute sur ce port ... la doc de sslh indique qu'un
temps de latence de 5 secondes au moins est nécessaire ... chez moi ça
ne fonctionne pas.

le port-sharing d'OpenVPN
-------------------------

Après quelques recherches je me suis aperçu qu'OpenVPN posséder une
option de port-sharing. J'ai donc laissé l'https d'apache configuré sur
le port 4443 comme je viens de l'expliquer plus haut et j'ai simplement
appliqué les modifications suivantes à la configuration d'OpenVPN via
l'interface d'OpenVPN Access Server

### modifier le port TCP d'écoute par défaut d'OpenVPN en 443

#### rubrique "Server Network Settings"

![openvpn as port sharing 1](images/openvpn-as-1.png)

Ajouter l'option port-share sur la même machine pour rerouter tout ce
qui arrive sur le port 443 et qui n'est pas du traffic OpenVPN sur le
port 4443 (le nouveau port https)

#### rubrique "Advanced VPN" tout en bas

![openvpn as port sharing 2](images/openvpn-as-2.png)

## C'est fait \o/

Une fois OpenVPN redémarré la magie a opéré: l'https continue de
fonctionner correctement de manière transparente (on peut en revanche
désormais y accéder directement via https://domain.tld:4443/) et OpenVPN
répond sur le port 443 ...

 

 

 

Title: PGP batman utilise GPG
Date: 2018-01-17 08:34
Author: mazenovi
Category: vie-privee
Tags: crypto, gpg
Status: draft

[TOC]

## Chiffrer / signer

### Chiffrer un message / un fichier avec une clé importée

```
gpg -a -o msg.gpg.asc -e -r themouette@keybase.io msg
```

```
pgp -d msg.gpg.asc
```

### Faire une signature et la vérifier

```
mazenovi@BigMaze:~/Bureau$ echo "Je déclare solonnellement que: 42." | gpg -a -s --clearsign

Une phrase secrète est nécessaire pour déverrouiller la clef secrète de
l'utilisateur : « Vincent Mazenod (clef PGP pour m4z3.me) <taulier@m4z3.me> »
clef RSA de 2048 bits, identifiant 9C96F4C5, créée le 2014-12-17

-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Je déclare solonnellement que: 42.
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAEBAgAGBQJYEGrMAAoJEPwxd2CclvTF8Z0IAI2Ey4V4woigf0KKX91QYrfv
reHwdT9jLDPY//izqWR8ShiQhOeW6DGeSYD2eesJieq+EQUWGoUYetsSwbO7Ign3
w+Oxs5XsD0p0MrZJHcUE8sIcxuDi8lw1iv2+ffFuiKPbi3s0nyfCCPevA4QQIuI9
xIlfE13C2iKAJ5JYD4Yc4o9CosfM+aazaY22RJwQLa3Sr57Au9HXSLlkTRbP8Aq4
ZACqF9dHqt6b9i4okE15yXndzjwQ/PQCDDIpi6/QfY7PG7OivsPGk/NqbGt3jclk
vqm37pvcOg7EyU1XbAwD0efrIdIxKA8zAtTZPG93B26MAM24OVIR6LaJTxABlmI=
=2w82
-----END PGP SIGNATURE-----
mazenovi@BigMaze:~/Bureau$ echo "Je déclare solonnellement que: 42." | gpg -a -s --clearsign > maze.sig

Une phrase secrète est nécessaire pour déverrouiller la clef secrète de
l'utilisateur : « Vincent Mazenod (clef PGP pour m4z3.me) <taulier@m4z3.me> »
clef RSA de 2048 bits, identifiant 9C96F4C5, créée le 2014-12-17

mazenovi@BigMaze:~/Bureau$ vi maze.sig
mazenovi@BigMaze:~/Bureau$ gpg --output sig --decrypt maze.sig
gpg: Signature faite le mer. 26 oct. 2016 10:35:55 CEST avec la clef RSA d'identifiant 9C96F4C5
gpg: Bonne signature de « Vincent Mazenod (clef PGP pour m4z3.me) <taulier@m4z3.me> »
gpg: Attention : cette clef n'est pas certifiée avec une signature de confiance.
gpg:          Rien n'indique que la signature appartient à son propriétaire.
Empreinte de clef principale : ACE7 302C DD02 2220 689F  4DDB FC31 7760 9C96 F4C5
```

### signer des documents

https://www.gnupg.org/gph/en/manual/x135.html

### enigmail

### k9mail

### openKeyChain

### openGPG.js

## Gérer sa toile de confiance

### Configurer un serveur de clef

Pour partager votre clef avec le monde entier, vous pouvez publier votre clef sur un serveur de clef public. Pour ce faire, vous devez configurer un serveur dans gpg, en éditant le fichier de configuration .gnupg/gpg.conf:

```
keyserver pool.sks-keyservers.net
```

ou directement dans la ligne de commande

```
 gpg --keyserver pgp.mit.edu --search-keys pascal.lafourcade@udamail.fr
```

https://keyserver.pgp.com

### Garder son trousseau à jour

gpg --refresh-keys

```
# mettre à jour mes clefs OpenPGP à chaque jour, à midi
0 12 * * * /usr/bin/gpg --refresh-keys > /dev/null 2>&1
```

### Importer un clé à partir d'un serveur de clé public (manuellement i.e. sans enigmail)

* http://pgp.mit.edu/
* http://pgp.mit.edu/pks/lookup?search=avez&op=index
* http://pgp.mit.edu/pks/lookup?op=vindex&search=0x5712572438B0CD9D
* http://pgp.mit.edu/pks/lookup?op=get&search=0x5712572438B0CD9D -> avez.pub

```
mazenovi@BigMaze:~/Bureau$ gpg --import avez.pub
gpg: clef 38B0CD9D : clef publique « Guillaume Avez - ISIMA <guillaume.avez@isima.fr> » importée
gpg: Quantité totale traitée : 1
gpg:               importées : 1  (RSA: 1)
mazenovi@BigMaze:~/Bureau$ gpg --list-keys
/home/mazenovi/.gnupg/pubring
---------------------------------
pub   2048R/9C96F4C5 2014-12-17 [expire : 2019-12-16]
uid                  Vincent Mazenod (clef PGP pour m4z3.me) <taulier@m4z3.me>
sub   2048R/8979C7D4 2014-12-17 [expire : 2019-12-16]

pub   4096R/38B0CD9D 2015-12-16 [expire : 2020-12-14]
uid                  Guillaume Avez - ISIMA <guillaume.avez@isima.fr>
sub   4096R/FAB15F60 2015-12-16 [expire : 2020-12-14]
```
1
la nouvelle clé est importée

### Signer une clé publique

http://openpgp.vie-privee.org/gpg-intro-5.html

https://www.gnupg.org/gph/en/manual/x56.html

gpg --edit-key info-clef

    1 = Je ne sais pas (I Don't know)
    2 = Je ne fais pas confiance (I do NOT trust)
    3 = Je fais un peu confiance (I trust marginally)
    4 = Je fais totalement confiance (I trust fully)

### Lister les clés publiques qui ont signés

    Command> check
    uid  Blake (Executioner) <blake@cyb.org>
    sig!       9E98BC16 1999-06-04   [self-signature]
    sig!       BB7576AC 1999-06-04   Alice (Judge) <alice@cyb.org>

## liens

* https://www.gnupg.org/howtos/fr/index.html
* https://wiki.koumbit.net/GnuPrivacyGuard
* https://www.gnupg.org/documentation/manuals/gnupg/GPG-Configuration.html
* https://kb.iu.edu/d/awiu
* https://openclassrooms.com/courses/la-cryptographie-facile-avec-pgp

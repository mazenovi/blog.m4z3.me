Title: Ne pas se faire trahir par son mot de passe en utilisant KeePass
Date: 2011-03-17 15:57
Author: mazenovi
Category: vie privée
Tags: open-source
Slug: ne-pas-se-faire-trahir-par-son-mot-de-passe-en-utilisant-keepass
Status: published

Avec le nouveau décret d'application de la LCEN on peut se demander[en
quoi le mot de passe peut servir à
l'identification](http://www.numerama.com/magazine/18214-pourquoi-les-mots-de-passe-peuvent-servir-a-identifier-un-internaute.html)?

L'idée est assez vicieuse mais valide en pratique: si vous voulez garder
l'anonymat vous penserez en premier lieu à vous créer  un compte bidon
avec un identifiant bidon du genre *papillon63* et pour ne pas vous
surcharger les neurones vous mettrez  votre mot de passe habituel:
*gro\$\$3cr3t*. Si on a des soupçons sur le fait que *mazenovi* est
*papillon 63* (et que *papillon63* est recherché pour cybercrime contre
la cyber humanité), il est évident que le fait que ces deux comptes
aient le même mot de passe où le même hash de mot de passe, va alourdir
le dossier.

**N.B.** Comme le rappelait Korben [la plus part des mot de passe sont
stockés
hachés](http://www.korben.info/votre-mot-de-passe-na-plus-de-secret-pour-le-gouvernement.html),
ce qui signifie qu'on ne peut pas connaitre le mot de passe original,
mais qu'on peut comparer la signature (le hash) de deux mots de passe,
dans la mesure où une chaîne de caractère hachée avec la même [fonction
de hachage](http://fr.wikipedia.org/wiki/Fonction_de_hachage)donne
toujours la même signature ...

Bref il est temps d'arrêter le mot de passe unique qui ouvre les portes
de tous ses comptes, qu'on ne révèlera jamais à personne même sous la
torture, et d'utiliser un gestionnaire de mots de passe digne de ce nom.

KeePass
=======

[KeePass](http://keepass.info/) va vous permettre de centraliser vos
mots de passe et d'y accéder via un mot de passe unique. Bien entendu on
pourra discuter le caractère sécuritaire de cette approche qui n'est
valable que tant que le mot de passe principal n'est pas violé.

Si le mot de passe général est deviné là c'est la vraie catastrophe
puisque TOUS vos mots de passe deviennent accessibles. Notez bien que ce
mot de passe n'est stocké en clair nulle part et qu'il est haché via
SHA-256 réputé inviolé à ce jour.

Une autre méthode d'ouverture, plus sécurisée, de la base de données de
mot de passe, est l'ouverture par fichier. vous devez avoir le fichier
pour ouvrir la base, ce fichier est alors bien entendu confidentiel.

Encore plus sécurisé mais plus risqué mot de passe et fichier peuvent
être combinés.

D'un point de vue pratique [KeePass](http://keepass.info/) crypte le
fichier complet de mot de passe (.kdb) et pas seulement les mots de
passe, via Only Rijndael (AES) ou Twofish. Tous les mots de passes
manipulés sont également cryptés en mémoire.

<div style="text-align: center; margin-bottom: 20px">
    <img src="images/keepass/main_big.png">
</div>

[KeePass](http://keepass.info/) est portable (&lt;3) et est disponible
sur la [LiberKey](http://www.liberkey.com/). J'en fait la promotion sous
cette forme car il est l'auto type est automatiquement configurer avec
le FireFox portable de la [LiberKey](http://www.liberkey.com/) ce qui
permt de réduire les traces laissées sur la machine hôte.

Autrement dit

-   ouvrez la page d'authentification à la quelle vous voulez vous
    connecter
-   Placer le curseur dans le champs du username
-   Faites un clic droit sur le compte associé dans KeePass, cliquez sur
    "saisie semi automatique"
-   laissez Keepass faire le reste ...

Pour plus de sécurité [KeePass](http://keepass.info/) permet de générer
des mots de passe de complexité variable.

Plus besoin de les retenir maintenant puisqu'ils sont gérés de manière
centralisée ...

Voilà il sera dorénavant difficile de vous confondre avec deux mots de
passe identiques

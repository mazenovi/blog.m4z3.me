from setuptools import setup

setup(
    name='blog-m4z3',
    version='1.0',
    include_package_data=True,
    install_requires=[
        'pelican',
        'pygments',
        'markdown',
        'beautifulsoup4',
        'typogrify'
    ]
)

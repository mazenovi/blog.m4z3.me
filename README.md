
## Installer les dépendances

```bash
sudo pip install --editable .
```

## Lancer le serveur de prévisualisation

```bash
bash develop_server.sh start
```

## see also

* [https://limos.isima.fr/~mazenod/blog-et-pages-perso-au-limos.html](https://limos.isima.fr/~mazenod/blog-et-pages-perso-au-limos.html)
